
# Deployment
## Docker-Compose (Datenbank & Django):

### Requirements

- docker-compose

```
docker-compose -f ./backend-docker-compose.yml up
```
Server erreichbar unter localhost:8080

## Ohne Docker-compose:

### Requirements

- phyton
- pip
- django
- (docker für Datenbank)

Ggf. muss die Config für den Datenbankzugriff in [`settings.py`](./betteranki/settings.py) angepasst werden.
```bash
# Datenbank starten
docker build -t database ./db/
docker run -p 3306:3306 database

# Python dependencies installieren
pip install -r requirements.txt

# Datenmodelle migrieren
python manage.py migrate

# Server starten
python manage.py runserver 8080
```



# Usage

Die Dokumentations der API ist unter http://localhost:8080/api/docs erreichbar.

Das Django Admin Panel kann unter http://localhost:8080/admin aufgerufen werden.

Der Standard Login lautet:
```
user: admin
password: 12345
```

Der Login für den Superuser kann im [`start.backend.sh`](./start.backend.sh) Skript geändert werden

```bash
echo 'creating superuser'
python manage.py ensure_adminuser --username=admin --email=admin@example.com --password=12345
```


# Development
Das Backend wurde in Python mithilfe von [Django](https://docs.djangoproject.com/en/4.1/) und dem Django Rest Framework [Django-Ninja](https://django-ninja.rest-framework.com/) umgesetzt.


## Struktur

```
backend
├── betteranki
│   ├── api.py      # von hier aus werden die Anfragen an die
|   |               # verschiedenen API-Controller in /main/api
|   |               # geroutet
│   ├── asgi.py
│   ├── settings.py # globale Config, z.B. für Email
│   ├── urls.py
│   └── wsgi.py
├── main
│   ├── admin.py            # Einstellungen für Django Admin Panel
│   ├── api                 # Sammlung der API Controller
│   │   ├── cards.py        # API-Controller für Lernkarten
│   │   ├── decks.py        # API-Controller für Decks
│   │   ├── metas.py        # ...
│   │   ├── schemes.py      # Schemes, die für alle Controller
|   |   |                   # verwendet werden
│   │   ├── tags.py
│   │   └── users.py
│   ├── apps.py
│   ├── __init__.py
│   ├── migrations          # hier werden Änderungen des Datenmodells
|   |   |                   # nach Migrierung festgehalten
│   │   ├── 0001_initial.py
│   │   ├── ... 
│   ├── models.py           # hier ist das Datenmodell definiert
│   ├── tests.py
│   ├── views.py
│   ...
│
├── README.md
├── requirements.txt        # Python dependencies
...

```

## Datenmodell
![Image](./db/er-diagram.png "a title")


## Anlegen einer neuen API Route

### Beispiel: GET für eine spezifische Karte

Neues File für API-Controller anlegen:
```
cd backend
touch ./main/api/cards.py
```

[main/api/cards.py](./main/api/cards.py)
```python
from django.shortcuts import get_object_or_404
from ninja_extra import api_controller, route


# Neues API Schema zur Ausgabe von Karten erstellen
# Hierzu wird ein ModelSchema verwendet. Dieses greift direkt
# auf das in models.py definierte Datenmodell der Card Klasse zu
class CardOut(ModelSchema):
    class Config:
        model = Card
        # Auswahl der Cards Attribute, welche für das Schema
        # verwendet werden soll
        model_fields = ['id', 'name', 'deck', 
                        'question_html', 'answer_html']

# Anlegen eines neuen API Controllers für die Route /cards
# Alle Endpunkte nutzen einen JSON-Webtoken zur Authentifikation
@api_controller("/cards", auth=JWTAuth())
class CardsController:
    
    # GET Route anlegen mit einem URL Parameter für die Card ID
    # in responses werden die jeweiligen HTTP return codes und
    # das dazu entsprechende Schema definiert
    @route.get("/{card_id}", response={200: CardOut, 403: Detail, 404: Detail})
    def get_card(self, request, card_id):
        # Nutzer aus Request lesen
        user = request.auth
        # Card Objekt aus Datenbank holen
        # wenn nicht vorhanden HTTP 404 zurücksenden
        card = get_object_or_404(Card, id=card_id)
        # prüfen, ob Nutzer berechtigt ist die Karte abzufragen
        if card.deck.user.id != user.id:
            return 403, {"detail": "This is not your card"}
        # Card Objekt mit HTTP 200 zurücksenden
        return 200, card

```

[betteranki/api.py](./betteranki/api.py)
```python
from ninja_extra import NinjaExtraAPI
from main.api.cards import CardsController

api = NinjaExtraAPI()

# Den eben erstellten CardsController zur API hinzufügen
api.register_controllers(CardsController)
```