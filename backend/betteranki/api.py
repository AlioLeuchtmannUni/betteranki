from ninja import NinjaAPI
from ninja_extra import NinjaExtraAPI
from ninja_jwt.controller import NinjaJWTDefaultController
from main.api.users import UsersController
from main.api.tags import TagsController
from main.api.cards import CardsController
from main.api.decks import DecksController
from main.api.metas import MetasController
from main.api.stats import StatsController

api = NinjaExtraAPI()

api.register_controllers(UsersController)
api.register_controllers(NinjaJWTDefaultController)
api.register_controllers(DecksController)
api.register_controllers(CardsController)
api.register_controllers(TagsController)
api.register_controllers(StatsController)
api.register_controllers(MetasController)
