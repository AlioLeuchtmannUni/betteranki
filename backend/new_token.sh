#!/bin/bash

# copy new access token for user admin into clipboard

curl -s -X 'POST' \
  'http://localhost:8080/api/token/pair' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "password": "12345",
  "username": "admin"
}' | jq -r .access | xsel -ib
