#!/bin/bash

cd code


echo 'installing requirements'
pip install -r requirements.txt

echo 'migrating database'
python manage.py migrate

echo 'creating superuser'
python manage.py ensure_adminuser --username=admin --email=admin@example.com --password=12345

echo 'creating systemuser'
python manage.py ensure_systemuser --password=12345

echo 'starting server'
python manage.py runserver 0.0.0.0:8080 # ACHTUNG 0.0.0.0 essentiell da in docker container -> will nicht localhost sondern broadcast

exit