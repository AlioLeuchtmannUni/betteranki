from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = "Creates an admin user non-interactively if it doesn't exist"

    def add_arguments(self, parser):
        parser.add_argument('--password', help="system-users password")

    def handle(self, *args, **options):
        User = get_user_model()
        if not User.objects.filter(username="system").exists():
            User.objects.create_superuser(username="system",
                                          email="system@betteranki.com",
                                          password=options['password'])
