import re
from .schemes import Detail, Success
from django.contrib.auth.models import User
from ninja import Router, Schema
from ninja_jwt.authentication import JWTAuth
from django.shortcuts import get_object_or_404
from ninja_extra import api_controller, route



def email_valid(email):
    # RFC5322-compliant email regex
    # https://stackabuse.com/python-validate-email-address-with-regular-expressions-regex/
    regex = re.compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')
    return re.fullmatch(regex, email)

def email_exists(email):
    return User.objects.filter(email=email).exists()

def username_exists(username):
    return User.objects.filter(username=username).exists()


def get_requested_user(req_username, logged_user):
    if req_username == 'me':
        return logged_user
    else:
        return get_object_or_404(User, username=req_username)


class UserIn(Schema):
    username: str
    email: str
    password: str

class UserOut(Schema):
    id: int
    username: str
    email: str


@api_controller("/users", auth=JWTAuth())
class UsersController:

    @route.post("/register", auth=None, response={201:UserOut, 400:Detail, 409:Detail})
    def create_user(self, request, payload:UserIn):
        if not email_valid(payload.email):
            return 400, {"detail": "Invalid email format"}
        if email_exists(payload.email):
            return 409, {"detail": "Email already exists"}
        if username_exists(payload.username):
            return 409, {"detail": "Username already exists"}
        if len(payload.password) < 5:
            return 400, {"detail": "Insufficient password length"}
        
        user = User(username=payload.username, email=payload.email)
        user.set_password(payload.password)
        user.save()
        return 201, user
        
    @route.get("", response={200: UserOut})
    def get_user(self, request):
        user = request.auth
        return user

    @route.delete("", response={200: Success})
    def delete_user(self, request):
        user = request.auth
        user.delete()
        return 200, {"success": True}

    @route.put("/password", response={200:Success, 400:Detail})
    def change_password(self, request, old_pw, new_pw):
        user = request.auth
        if len(old_pw) < 1 or len(new_pw) < 5:
            return 400, {"detail": "Insufficient password length"}
        if not user.check_password(old_pw):
            return 400, {"detail": "Incorrect old password"}
        user.set_password(new_pw)
        user.save()
        return 200, {"success": True}
