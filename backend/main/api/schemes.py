from ninja import Schema, ModelSchema
from .. import models
from typing import List

class Detail(Schema):
    detail: str

class Success(Schema):
    success: bool
