from ..models import Card, CardMeta
from ninja import Schema
from ninja_jwt.authentication import JWTAuth
from typing import List
from django.shortcuts import get_object_or_404
from django.db.models import Count
from django.db.models.functions import Cast
from django.db.models.fields import DateField
from ninja_extra import api_controller, route
from django.contrib.auth.models import User
from datetime import date, timedelta


class StatsSchema(Schema):
    descriptor: str
    value: float


class StatsList(Schema):
    stats: List[StatsSchema]


class Stats:
    descriptor = None
    value = None

    def __init__(self, descriptor, value) -> None:
        self.descriptor = descriptor
        self.value = value


def one_week_ago():
    today = date.today()
    return (today - timedelta(days=7))


@api_controller("/stats", auth=JWTAuth())
class StatsController:

    @route.get("/card-count", response=StatsList)
    def card_count(self, request):
        user = request.auth
        data = Card.objects\
            .prefetch_related("deck")\
            .filter(deck__user=user)\
            .values("deck__name")\
            .annotate(count=Count("deck"))\
            .order_by("-count")
        stats = [Stats(d.get("deck__name"), d.get("count")) for d in data]
        return 200, {"stats": stats}

    @route.get("/learned", response=StatsList)
    def learned_cards(self, request, start: date = one_week_ago(), end: date = date.today()):
        # date filter end-date in django queryset is excluding
        end += timedelta(days=1)
        delta = (end-start).days
        user = request.auth
        data = CardMeta.objects\
            .prefetch_related("card", "card__deck")\
            .filter(card__deck__user=user)\
            .filter(timestamp__range=[start, end])\
            .annotate(date=Cast("timestamp", DateField()))\
            .values("date")\
            .annotate(count=Count("date"))

        # add days with count 0 to the data
        dates = [start + timedelta(days=x) for x in range(delta)]
        stats = []
        data_index = 0
        for d in dates:
            current_data = data[data_index]
            # check if d is in data
            if current_data.get("date") == d:
                # add data item as Stats object to stats list
                stats.append(Stats(str(current_data.get("date")),
                                   current_data.get("count")))
                # avoid data_index out of range
                if data_index < len(data)-1:
                    data_index += 1
            else:
                # create Stats object with count 0
                # and add it to stats list
                stats.append(Stats(str(d), 0))
        return 200, {"stats": stats}

    @route.get("/scores", response=StatsList)
    def card_scores(self, request, start: date = one_week_ago(), end: date = date.today()):
        # date filter end-date in django queryset is excluding
        end += timedelta(days=1)
        user = request.auth
        data = CardMeta.objects\
            .prefetch_related("card", "card__deck")\
            .filter(card__deck__user=user)\
            .filter(timestamp__range=[start, end])\
            .values("score")\
            .annotate(count=Count("score"))\
            .order_by("score")
        stats = []
        for item in data:
            score = item.get("score")
            count = item.get("count")
            s = Stats(
                CardMeta.SCORE_CHOICES[score][1],  # again, hard, good or easy
                count
            )
            stats.append(s)
        return 200, {"stats": stats}
