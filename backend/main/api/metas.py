from ..models import CardMeta, Card
from .schemes import Detail, Success
from ninja import Schema, ModelSchema
from ninja_jwt.authentication import JWTAuth
from django.shortcuts import get_object_or_404
from ninja_extra import api_controller, route
from typing import List

class MetaOut(ModelSchema):
    class Config:
        model = CardMeta
        model_fields = ['id', 'card', 'timestamp', 'score']

class MetasOut(Schema):
    metas: List[MetaOut]

class MetaIn(ModelSchema):
    class Config:
        model = CardMeta
        model_fields = ['card', 'score']

def validate_score(score) -> bool:
    return score >= 0 and score <= 3


@api_controller("/metas", auth=JWTAuth())
class MetasController:
    @route.get("", response=MetasOut)
    def get_all_metas(self, request):
        user = request.auth
        metas = list(CardMeta.objects.filter(card__deck__user=user))
        return 200, {"metas": metas}
    
    @route.get("/{id}", response={200: MetaOut, 403: Detail, 404: Detail})
    def get_meta(self, request, id):
        user = request.auth
        meta = get_object_or_404(CardMeta, id=id)
        if meta.card.deck.user.id != user.id:
            return 403, {"detail": "This is not your Card Metadata"}
        return 200, meta

    
    @route.post("", response={201: MetaOut, 400: Detail, 403: Detail})
    def create_meta(self, request, payload: MetaIn):
        user = request.auth
        card = get_object_or_404(Card, id=payload.card)
        if card.deck.user.id != user.id:
            return 403, {"detail": "This is not your Card"}
        if not validate_score(payload.score):
            return 400, {"detail": "Score must be between 0 and 3"}
        meta = CardMeta.objects.create(card=card, score=payload.score)
        return 201, meta
    
    @route.put("/{id}", response={200: MetaOut, 400: Detail, 403: Detail, 404: Detail})
    def update_meta(self, request, id, payload: MetaIn):
        user = request.auth
        meta = get_object_or_404(CardMeta, id=id)
        if meta.card.deck.user.id != user.id:
            return 403, {"detail": "This is not your Card Metadata"}
        if not validate_score(payload.score):
            return 400, {"detail": "Score must be between 0 and 3"}
        meta.card = payload.card
        meta.score = payload.score
        meta.save()
        return 200, meta
    
    @route.delete("/{id}", response={200: Success, 403: Detail, 404: Detail})
    def delete_meta(self, request, id):
        user = request.auth
        meta = get_object_or_404(CardMeta, id=id)
        if meta.card.deck.user.id != user.id:
            return 403, {"detail": "This is not your Card Metadata"}
        meta.delete()
        return 200, {"success": True}