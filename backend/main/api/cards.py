from ..models import Card
from .schemes import Detail, Success
from ninja import Schema, ModelSchema
from ninja_jwt.authentication import JWTAuth
from typing import List
from django.shortcuts import get_object_or_404
from ninja_extra import api_controller, route


class CardOut(ModelSchema):
    class Config:
        model = Card
        model_fields = ['id', 'name', 'deck', 'question_html', 'answer_html']


class CardsOut(Schema):
    cards: List[CardOut]


class CardIn(ModelSchema):
    class Config:
        model = Card
        model_fields = ['name', 'deck', 'question_html', 'answer_html']


@api_controller("/cards", auth=JWTAuth())
class CardsController:

    @route.get("", response={200: CardsOut, 403: Detail, 404: Detail})
    def get_all_cards(self, request):
        user = request.auth
        cards = list(Card.objects.filter(deck__user=user))
        return 200, {"cards": cards}

    @route.get("/{card_id}", response={200: CardOut, 403: Detail, 404: Detail})
    def get_card(self, request, card_id):
        user = request.auth
        card = get_object_or_404(Card, id=card_id)
        if card.deck.user.id != user.id:
            return 403, {"detail": "This is not your card"}
        return 200, card

    @route.put("/{card_id}", response={200: CardOut, 403: Detail, 404: Detail})
    def update_card(self, request, card_id, payload: CardIn):
        user = request.auth
        card = get_object_or_404(Card, id=card_id)
        if card.deck.user.id != user.id:
            return 403, {"detail": "This is not your card"}
        card.name = payload.name
        card.question_html = payload.question_html
        card.answer_html = payload.answer_html
        card.save()
        return 200, card

    @route.delete("/{card_id}", response={200: Success, 403: Detail, 404: Detail})
    def delete_card(self, request, card_id):
        user = request.auth
        card = get_object_or_404(Card, id=card_id)
        if card.deck.user.id != user.id:
            return 403, {"detail": "This is not your card"}
        card.delete()
        return 200, {"success": True}
