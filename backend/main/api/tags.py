from ..models import Tag
from .schemes import Detail, Success
from ninja import Schema, ModelSchema
from ninja_jwt.authentication import JWTAuth
from typing import List
from django.shortcuts import get_object_or_404
from ninja_extra import api_controller, route


class TagOut(ModelSchema):
    class Config:
        model = Tag
        model_fields = ['id', 'name']


class TagsOut(Schema):
    tags: List[TagOut]


class TagIn(ModelSchema):
    class Config:
        model = Tag
        model_fields = ['name']


@api_controller("/tags", auth=JWTAuth())
class TagsController:

    @route.get("", response={200: TagsOut})
    def get_all_tags(self, request):
        user = request.auth
        tags = list(Tag.objects.filter(user=user))
        return 200, {"tags": tags}

    @route.get("/{tag_id}", response={200: TagOut, 403: Detail, 404: Detail})
    def get_tag(self, request, tag_id):
        user = request.auth
        tag = get_object_or_404(Tag, id=tag_id)
        if tag.user.id != user.id:
            return 403, {"detail": "This is not your Tag"}
        return 200, tag

    @route.post("", response={201: TagOut, 403: Detail})
    def create_tag(self, request, payload: TagIn):
        user = request.auth
        tag = user.tag_set.create(name=payload.name)
        return 201, tag

    @route.put("/{tag_id}", response={200: TagOut, 403: Detail, 404: Detail})
    def update_tag(self, request, tag_id, payload: TagIn):
        user = request.auth
        tag = get_object_or_404(Tag, id=tag_id)
        if tag.user.id != user.id:
            return 403, {"detail": "This is not your Tag"}
        tag.name = payload.name
        tag.save()
        return 200, tag

    @route.delete("/{tag_id}", response={200: Success, 403: Detail, 404: Detail})
    def delete_tag(self, request, tag_id):
        user = request.auth
        tag = get_object_or_404(Tag, id=tag_id)
        if tag.user.id != user.id:
            return 403, {"detail": "This is not your Tag"}
        tag.delete()
        return 200, {"success": True}
