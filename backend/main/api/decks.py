from ..models import Deck, Card, Tag
from .schemes import Detail, Success
from .cards import CardIn, CardOut
from .tags import TagOut
from ninja import ModelSchema, Schema
from ninja_jwt.authentication import JWTAuth
from typing import List
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from django.db import transaction
from ninja_extra import api_controller, route
import copy


class DeckOut(ModelSchema):
    tags: List[TagOut]
    cards: List[CardOut]

    class Config:
        model = Deck
        model_fields = ['id', 'name', 'description', 'tags',
                        'user', 'favourite', 'shared', 'original_deck']

    def resolve_cards(self, deck):
        return Card.objects.filter(deck=deck)


class DecksOut(Schema):
    decks: List[DeckOut]


class DeckIn(ModelSchema):
    tags: List[TagOut]

    class Config:
        model = Deck
        model_fields = ['name', 'description', 'tags', 'favourite']


class Share(Schema):
    shared: bool


def deck_exists(name, user) -> bool:
    return Deck.objects.filter(user=user, name=name).exists()


# returns a list of tag ids for a given list of tag objects
def get_tag_ids(tags: list[Tag]) -> list[int]:
    return [tag.id for tag in tags]


# filter tags having "id": null
def filter_new_tags(tags: list[TagOut]) -> list[TagOut]:
    return [tag for tag in tags if tag.id == None]


# if there are new tags, save them to db and add them
# to passed list so they can be added to deck.tags later
def create_new_tags(tags, user, all_tag_ids: list[int]):
    new_tags = filter_new_tags(tags)
    if new_tags:
        tag_objects = [Tag(name=tag.name, user=user) for tag in new_tags]
        for tag in tag_objects:
            tag.save()
        new_tag_ids = get_tag_ids(tag_objects)
        all_tag_ids.extend(new_tag_ids)


# copy all cards from a given deck to another
@transaction.atomic
def copy_cards(original_deck: Deck, new_deck: Deck):
    cards = Card.objects.filter(deck__id=original_deck.id)
    for card in cards:
        card.id = None
        card.deck = new_deck
    Card.objects.bulk_create(cards)


@transaction.atomic
def copy_deck(deck: Deck, new_user: User, share: bool = False):
    new_deck = copy.copy(deck)
    new_deck.id = None
    new_deck.favourite = False
    new_deck.user = new_user
    new_deck.shared = False
    if share:
        # add foreign key to original deck if copy_deck function
        # is called during a share operation
        new_deck.original_deck = deck
        new_deck.shared = True
    new_deck.save()
    copy_cards(original_deck=deck, new_deck=new_deck)


def deck_is_shared(deck: Deck):
    system_user = User.objects.get(username="system")
    return Deck.objects.filter(user=system_user, original_deck=deck).exists()


@api_controller("/decks", auth=JWTAuth())
class DecksController:

    # Deck Sharing

    @route.get("/shared", response={200: DecksOut, 403: Detail}, auth=None)
    def get_shared_decks(self, request):
        system_user = User.objects.get(username="system")
        decks = list(system_user.deck_set.all())
        return 200, {"decks": decks}

    @route.get("shared/my", response={200: DecksOut})
    def get_my_shared_decks(self, request):
        user = request.auth
        system_user = User.objects.get(username="system")
        decks = list(Deck.objects.filter(
            user=system_user, original_deck__user=user))
        return 200, {"decks": decks}

    @route.get("/shared/{deck_id}/copy", response={200: Success})
    def copy_shared_deck(self, request, deck_id):
        system_user = User.objects.get(username="system")
        user = request.auth
        shared_deck = get_object_or_404(Deck, id=deck_id)
        copy_deck(shared_deck, user)
        return 200, {"success": True}

    @route.put("/share/{deck_id}", response={200: Success, 409: Detail})
    def share_deck(self, request, deck_id, payload: Share):
        system_user = User.objects.get(username="system")
        user = request.auth
        deck = get_object_or_404(Deck, id=deck_id)
        if deck.user.id != user.id:
            return 403, {"detail": "This is not your deck"}
        if payload.shared:
            if deck_is_shared(deck):
                return 409, {"detail": "Deck is already being shared"}
            copy_deck(deck, system_user, True)
            deck.shared = True
            deck.save()
            return 200, {"success": True}
        else:
            if not deck_is_shared(deck):
                return 409, {"detail": "Deck not being shared"}
            shared_deck = Deck.objects.filter(
                user=system_user, original_deck=deck)
            shared_deck.delete()
            deck.shared = False
            deck.save()
            return 200, {"success": True}

    # User Deck CRUD

    @route.get("", response={200: DecksOut, 403: Detail})
    def get_all_decks(self, request):
        user = request.auth
        decks = list(user.deck_set.all())
        return 200, {"decks": decks}

    @route.get("/{deck_id}", response={200: DeckOut, 403: Detail, 404: Detail})
    def get_deck(self, request, deck_id):
        system_user = User.objects.get(username="system")
        user = request.auth
        deck = get_object_or_404(Deck, id=deck_id)

        if (deck.user.username == "system"):
            return 200, system_user.deck_set.get(id=deck_id)

        if (deck.user.id != user.id):
            return 403, {"detail": "This is not your deck"}

        return 200, user.deck_set.get(id=deck_id)

    @route.post("", response={201: DeckOut, 409: Detail})
    def create_deck(self, request, payload: DeckIn):
        user = request.auth
        if deck_exists(payload.name, user):
            return 409, {"detail": "Deck already exists"}
        deck = Deck(name=payload.name,
                    description=payload.description, user=user)
        # initial saving is required to obtain an id
        # adding related tags to a deck object without id is not allowed
        deck.save()
        tag_ids = get_tag_ids(payload.tags)
        create_new_tags(payload.tags, user, tag_ids)
        deck.tags.add(*tag_ids)
        deck.save()
        return 201, deck

    @route.put("/{deck_id}", response={200: DeckOut, 403: Detail, 404: Detail})
    def update_deck(self, request, deck_id, payload: DeckIn):
        user = request.auth
        deck = get_object_or_404(Deck, id=deck_id)
        if deck.user.id != user.id:
            return 403, {"detail": "This is not your deck"}
        deck.name = payload.name
        deck.description = payload.description
        deck.favourite = payload.favourite
        # remove all related tags and add only these
        # which are included in the put payload afterwards
        deck.tags.clear()
        tag_ids = get_tag_ids(payload.tags)
        create_new_tags(payload.tags, user, tag_ids)
        deck.tags.add(*tag_ids)
        deck.save()
        return 200, deck

    @route.delete("/{deck_id}", response={200: Success, 403: Detail, 404: Detail})
    def delete_deck(self, request, deck_id):
        user = request.auth
        deck = get_object_or_404(Deck, id=deck_id)
        if deck.user.id != user.id:
            return 403, {"detail": "This is not your deck"}
        deck.delete()
        return 200, {"success": True}

    @route.get("/{deck_id}/cards", response={200: List[CardOut], 403: Detail, 404: Detail})
    def get_all_cards_by_deck(self, request, deck_id):
        system_user = User.objects.get(username="system")
        user = request.auth
        deck = get_object_or_404(Deck, id=deck_id)

        if (deck.user.username == "system"):
            return 200, deck.card_set.all()

        if deck.user.id != user.id:
            return 403, {"detail": "This is not your deck"}

        return 200, deck.card_set.all()

    @route.post("/{deck_id}/cards", response={201: CardOut, 403: Detail, 404: Detail})
    def create_card_by_deck(self, request, deck_id, payload: CardIn):
        user = request.auth
        deck = get_object_or_404(Deck, id=deck_id)
        if deck.user != user:
            return 403, {"detail": "This is not your deck"}
        card = deck.card_set.create(
            name=payload.name,
            question_html=payload.question_html,
            answer_html=payload.answer_html
        )
        return 201, card
