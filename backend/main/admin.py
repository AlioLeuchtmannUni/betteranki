from django.contrib import admin
from . import models


@admin.register(models.Deck)
class DeckAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'description', 'related_tags', 'favourite', 'original_deck', 'id')
    list_select_related = ('user',)
    search_fields = ('name', 'description', 'user__username', 'id')
    ordering = ('name',)

    def related_tags(self, deck):
        return ", ".join([tag.name for tag in deck.tags.all()])


@admin.register(models.Card)
class CardAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'user', 'deck_name', 'question', 'answer')
    list_select_related = ('deck', 'deck__user')
    search_fields = ('name', 'question_html', 'answer_html', 'deck__name', 'deck__user__username', 'id')
    ordering = ('id',)

    def user(self, card):
        return card.deck.user
    user.admin_order_field = 'deck__user'

    def deck_name(self, card):
        return card.deck.name
    deck_name.admin_order_field = 'deck__name'

    # return first 15 characters of question
    def question(self, card):
        if len(card.question_html) <= 15:
            return card.question_html
        return card.question_html[:15] + "..."

    # return first 15 characters of answer
    def answer(self, card):
        if len(card.answer_html) <= 15:
            return card.answer_html
        return card.answer_html[:15] + "..."

@admin.register(models.Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'related_decks', 'id')
    list_select_related = ('user',)
    search_fields = ('name', 'user__username', 'id')
    ordering = ('name',)

    def related_decks(self, tag):
        return ", ".join([deck.name for deck in tag.deck_set.all()])


@admin.register(models.CardMeta)
class CardMetaAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'related_card', 'timestamp', 'score')
    list_select_related = ('card', 'card__deck', 'card__deck__user')
    search_fields = ('card__name', 'card__deck__name', 'card__deck__user__username', 'id')
    ordering = ('id',)

    def user(self, meta):
        return meta.card.deck.user
    user.admin_order_field = 'card__deck__user'

    def related_card(self, meta):
        return f"id: {meta.card.id} - name: {meta.card.name}"
    related_card.admin_order_field = 'card__id'

