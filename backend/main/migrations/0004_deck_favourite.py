# Generated by Django 4.1.2 on 2022-12-10 15:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_card_name_default_blank'),
    ]

    operations = [
        migrations.AddField(
            model_name='deck',
            name='favourite',
            field=models.BooleanField(default=False),
        ),
    ]
