# Generated by Django 4.1.3 on 2023-01-16 14:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_deck_original_deck'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='deck',
            unique_together=set(),
        ),
    ]
