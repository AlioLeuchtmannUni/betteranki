from django.db import models
from django.contrib.auth.models import User


class Tag(models.Model):  # Mathe
    name = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"Tag - name: {self.name}"

    class Meta:
        unique_together = ('name', 'user')


class Deck(models.Model):  # Algebra, Stochastik
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    favourite = models.BooleanField(default=False)
    shared = models.BooleanField(default=False)
    original_deck = models.ForeignKey(
        'Deck',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    def __str__(self) -> str:
        return f"{self.name} - {self.user.username}"


class Card(models.Model):  # Lernkarte
    name = models.CharField(max_length=255, blank=True, null=True)
    deck = models.ForeignKey(Deck, on_delete=models.CASCADE)
    question_html = models.TextField()
    answer_html = models.TextField()

    def __str__(self) -> str:
        return f"{self.id} - name: {self.name}, q: {self.question_html[:10]}, a: {self.answer_html[:10]}"


class CardMeta(models.Model):
    SCORE_CHOICES = [
        (0, "again"),
        (1, "hard"),
        (2, "good"),
        (3, "easy"),
    ]
    card = models.ForeignKey(Card, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    score = models.IntegerField(choices=SCORE_CHOICES)

    def __str__(self) -> str:
        return f"CardMeta - card: {self.card}, score: {self.score}"
