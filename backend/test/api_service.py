#!/usr/bin/env python3

import requests

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from test.login_service import LoginService

from test.utility import *

class ApiService:

    api_url = "http://localhost:8080/api"

    @staticmethod
    def get_endpoint_full_url(endpoint_relative_path: str) -> str:
        # endpoint_relative_path = endpoint_relative_path if endpoint_relative_path.endswith("/") else f"{endpoint_relative_path}/"
        
        return f"{ApiService.api_url}/{endpoint_relative_path}"
    
    @staticmethod
    def get_request_with_authentication(endpoint_relative_path: str, expected_status_code: int = 200) -> requests.Response:
        from test.login_service import LoginService

        endpoint_absolute_path = ApiService.get_endpoint_full_url(endpoint_relative_path)

        response = requests.get(endpoint_absolute_path, headers={"Authorization": f"Bearer {LoginService.get_jwt_token()}"})

        if response.status_code != expected_status_code:
            eprint(f"Got status code: {response.status_code} but expected {expected_status_code}")
            eprint(response.text)
            raise Exception

        return response
    
    @staticmethod
    def post_request_with_authentication(endpoint_relative_path: str, data, expected_status_code: int = 200) -> requests.Response:
        from test.login_service import LoginService

        endpoint_absolute_path = ApiService.get_endpoint_full_url(endpoint_relative_path)

        response = requests.post(endpoint_absolute_path, json=data, headers={"accept": "application/json", "Content-Type": "application/json", "Authorization": f"Bearer {LoginService.get_jwt_token()}"})

        if response.status_code != expected_status_code:
            eprint(f"Got status code: {response.status_code} but expected {expected_status_code}")
            eprint(response.text)
            raise Exception

        return response

    @staticmethod
    def delete_request_with_authentication(endpoint_relative_path: str, data, expected_status_code: int = 200) -> requests.Response:
        from test.login_service import LoginService

        endpoint_absolute_path = ApiService.get_endpoint_full_url(endpoint_relative_path)

        response = requests.delete(endpoint_absolute_path, json=data, headers={"accept": "application/json", "Content-Type": "application/json", "Authorization": f"Bearer {LoginService.get_jwt_token()}"})

        if response.status_code != expected_status_code:
            eprint(f"Got status code: {response.status_code} but expected {expected_status_code}")
            eprint(response.text)

            raise Exception

        return response
