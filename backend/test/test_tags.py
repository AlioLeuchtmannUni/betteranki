#!/usr/bin/env python3

import unittest
import requests

from test.login_service import LoginService
from test.api_service import ApiService

from test.utility import *

class Testtags(unittest.TestCase):

    create_new_tag_path = "tags"
    delete_tag_path = "tags"

    test_tag_name = "UnittestTagABC"

    def setUp(self):
        if not LoginService.refresh_jwt_token():
            raise Exception

        self.example_tag_json = {"name": self.test_tag_name}


    def test_tagAddAndDelete(self):
        response = ApiService.post_request_with_authentication(Testtags.create_new_tag_path, self.example_tag_json, expected_status_code=201)

        json_response = response.json()

        self.assertTrue("id" in json_response)
        self.assertTrue("name" in json_response)
        
        self.assertIsInstance(json_response.get("id"), int)
        self.assertIsInstance(json_response.get("name"), str)

        tag_id = json_response.get("id")
        tag_name = json_response.get("name")

        self.assertEqual(self.test_tag_name, tag_name)

        response = ApiService.delete_request_with_authentication(f"{Testtags.delete_tag_path}/{tag_id}", None)







