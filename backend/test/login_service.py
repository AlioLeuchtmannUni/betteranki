#!/usr/bin/env python3

from typing import Tuple
import requests
from test.api_service import ApiService

class LoginService:

    register_path = "users/register"
    login_path = "token/pair"

    test_username = "unittest123456"
    test_email = "unittest123456@test.com"
    test_password = "1234568ABCdef!"

    jwt_token = ""

    @staticmethod
    def get_jwt_token() -> str:
        if LoginService.jwt_token == "":
            LoginService.refresh_jwt_token
        
        return LoginService.jwt_token

    @staticmethod
    def refresh_jwt_token() -> bool:
        LoginService.register()

        success, refresh_token = LoginService.login()

        if success:
            LoginService.jwt_token = refresh_token
            return True

        return False

    @staticmethod
    def register() -> None:
        requests.post(ApiService.get_endpoint_full_url(LoginService.register_path), json={"username": LoginService.test_username, "email": LoginService.test_email, "password": LoginService.test_password})

    @staticmethod
    def login() -> Tuple[bool, str]:
        response = requests.post(ApiService.get_endpoint_full_url(LoginService.login_path), json={"username": LoginService.test_username, "password": LoginService.test_password})

        if response.status_code == 200:
            return True, response.json().get("access")

        return False, ""




