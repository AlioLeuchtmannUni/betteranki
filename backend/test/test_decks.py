#!/usr/bin/env python3

import unittest
import requests

from test.login_service import LoginService
from test.api_service import ApiService
from test.deck_service import DeckService

from test.utility import *

class TestDecks(unittest.TestCase):

    get_all_decks_path = "decks"
    create_new_deck_path = "decks"
    delete_deck_path = "decks"

    def setUp(self):
        if not LoginService.refresh_jwt_token():
            raise Exception
        
        self.example_deck_json = DeckService.get_deck_json("Testdeck", "My deck description.", [], False)

    def test_DeckGet(self):
        response = ApiService.get_request_with_authentication(TestDecks.get_all_decks_path)

        json_response = response.json()

        self.assertTrue("decks" in json_response)
        self.assertIsNotNone(json_response["decks"])
        self.assertTrue(is_list(json_response["decks"]))
    
    def test_DeckAddAndDelete(self):
        response = ApiService.post_request_with_authentication(TestDecks.create_new_deck_path, self.example_deck_json, 201)

        json_response = response.json()

        self.assertTrue("id" in json_response)
        self.assertTrue("name" in json_response)
        self.assertTrue("description" in json_response)
        self.assertTrue("tags" in json_response)
        self.assertTrue("user" in json_response)
        self.assertTrue("favourite" in json_response)
        self.assertTrue("cards" in json_response)
        self.assertTrue("original_deck" in json_response)

        self.assertIsInstance(json_response.get("id"), int)
        self.assertIsInstance(json_response.get("name"), str)
        self.assertIsInstance(json_response.get("user"), int)

        deck_id = json_response.get("id")

        self.assertEqual(self.example_deck_json.get("name"), json_response.get("name"))

        response = ApiService.delete_request_with_authentication(f"{TestDecks.delete_deck_path}/{deck_id}", None)

