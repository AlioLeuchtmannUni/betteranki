#!/usr/bin/env python3

import sys


def is_list(object: any) -> bool:
    return hasattr(object, "__len__")

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)