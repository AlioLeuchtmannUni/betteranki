#!/usr/bin/env python3

from typing import Container
from typing import TypedDict

class Tag(TypedDict):
    id: int
    name: str

class DeckService:

    @staticmethod
    def get_deck_json(name: str, description: str, tags: Container[Tag], favorite: bool):
        return {"name": name, "description": description, "tags": tags, "favourite": favorite}