
import { IonFab, IonFabButton, IonIcon,IonFabList} from '@ionic/vue';
import { Options, Vue} from 'vue-class-component';
import {Prop} from "vue-property-decorator";
import ActionButton from "@/models/actionButton";

@Options({
    components: {
        IonFab,
        IonFabButton,
        IonIcon,
        IonFabList,
    },
})
export default class FloatingActionSheetButton extends Vue {
    @Prop({default:[]}) actions!: ActionButton[];

}
