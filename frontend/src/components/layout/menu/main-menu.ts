
import {
    IonButton,
    IonMenu,
    IonContent,
    IonList,
    IonItem,
    IonToolbar,
    IonTitle,
    IonHeader,
    IonIcon, menuController
} from '@ionic/vue';
import { Options, Vue} from 'vue-class-component';
import './main-menu.css'
import TokenService from "@/services/token.service";
import {VModel} from "vue-property-decorator";
import AccountService from "@/services/account.service";

@Options({
    components: {
        IonButton,
        IonMenu,
        IonContent,
        IonList,
        IonItem,
        IonToolbar,
        IonTitle,
        IonHeader,
        IonIcon,
    },
})

export default class MainMenu extends Vue {

    @VModel() isLoggedIn = true;

    public mounted(): void{
        AccountService
            .loggedInStatus
            .addEventListener(
                'update',
                (res: any) => {
                    this.isLoggedIn = res.detail;
                }
            );
    }

    public toggleMenu(): void {
        menuController.toggle('main-menu');
    }

    public async goLogin(): Promise<void>{
        const res = await menuController.close('main-menu');
        this.$router.push('/login').then(_=>{console.log('go login');});
    }

    public async goHome(): Promise<void>{
        const res = await menuController.close('main-menu');
        this.$router.push('/').then(_=>{console.log('go home');});
    }

    public async goDecks(): Promise<void>{
        const res = await menuController.close('main-menu');
        this.$router.push('/decks').then(_=>{console.log('go decks');});
    }

    public async goDiscover(): Promise<void>{
        const res = await menuController.close('main-menu');
        this.$router.push('/discover').then(_=>{console.log('go discover');});
    }

    public async goDashboard(): Promise<void>{
        const res = await menuController.close('main-menu');
        this.$router.push('/dashboard').then(_=>{console.log('go dash');});
    }


}
