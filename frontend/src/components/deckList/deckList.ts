
import {
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonCheckbox,
    IonChip,
    IonCol,
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonPopover,
    IonRow,
    IonSlide,
    IonSlides,
    IonAccordion,
    IonAccordionGroup,
} from '@ionic/vue';
import { Options, Vue} from 'vue-class-component';
import {Prop, VModel} from "vue-property-decorator";
import Deck from '@/models/deck.model';
import {DeckItemAction} from "@/models/deckItemAction";
import DeckItem from '@/components/deckItem/deck-item.vue';
import {DefineComponent} from "vue";
const deckItem = DeckItem as unknown as DefineComponent;

@Options({
    components: {
        IonCheckbox,
        IonCol,
        IonCard,
        IonItem,
        IonLabel,
        IonRow,
        IonIcon,
        IonChip,
        IonList,
        IonSlides,
        IonSlide,
        IonContent,
        IonCardHeader,
        IonCardTitle,
        IonCardSubtitle,
        IonCardContent,
        IonPopover,
        IonAccordion,
        IonAccordionGroup,
        deckItem,
    },
})

export default class DeckList extends Vue {

    @Prop({default: []}) decks!: Deck[];
    @Prop() deckItemActions!: DeckItemAction[];
    @Prop() favouriteCallback: VoidFunction | undefined;
    @Prop({default: false}) saveInsteadOfFavourite: VoidFunction | undefined;

    public mounted(){
        console.log("test ");
    }




}

