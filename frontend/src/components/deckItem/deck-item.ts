
import {
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonCheckbox,
    IonChip,
    IonCol,
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonPopover,
    IonRow,
    IonSlide,
    IonSlides,
    IonAccordion,
    IonAccordionGroup,
} from '@ionic/vue';
import { Options, Vue} from 'vue-class-component';
import {Prop, VModel} from "vue-property-decorator";
import Deck from '@/models/deck.model';
import './deck-item.css';
import TagList from "@/components/tagList/tagList.vue";
import {DefineComponent} from "vue";
import DeckService from "@/services/deck.service";
import {DeckItemAction} from "@/models/deckItemAction";
import ToastService from "@/services/toast.service";

const tagList = TagList as unknown as DefineComponent;

@Options({
    components: {
        IonCheckbox,
        IonCol,
        IonCard,
        IonItem,
        IonLabel,
        IonRow,
        IonIcon,
        IonChip,
        IonList,
        IonSlides,
        IonSlide,
        IonContent,
        IonCardHeader,
        IonCardTitle,
        IonCardSubtitle,
        IonCardContent,
        IonPopover,
        IonAccordion,
        IonAccordionGroup,
        tagList,
    },
})

export default class DeckItem extends Vue {

    @VModel() isFavourite = false;
    @Prop() deck!: Deck;
    @Prop() deckItemActions!: DeckItemAction[];
    @Prop() favouriteCallback: VoidFunction | undefined;
    @Prop({default: false}) saveInsteadOfFavourite: VoidFunction | undefined;

    private deckService: DeckService = new DeckService();

    public mounted(){
        this.isFavourite = this.deck.favourite;
    }

    public onFavouriteClicked(event: any): void{
        event.stopPropagation();

        this.isFavourite = !this.isFavourite;
        this.deck.favourite = this.isFavourite;
        this.deckService.update(this.deck).then((res) => console.log(res));
        if(this.favouriteCallback !== undefined){
            this.favouriteCallback();
        }
    }

    public onSaveClicked(event: any): void{
        event.stopPropagation();
        this.deckService
            .getCopyOfSharedDeck(this.deck.id!)
            .then(
                (res) => new ToastService().presentToast("bottom", "Public Deck has been added to your Decks").then(),
                (err) => console.log(err)
            );

    }

    public stopPropagation(event: any): void {
        event.stopPropagation();
    }
}

