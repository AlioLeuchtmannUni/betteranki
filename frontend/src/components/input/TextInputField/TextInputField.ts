

import { IonInput, IonItem, IonLabel, IonNote } from '@ionic/vue';
import {Options, Vue} from "vue-class-component";
import {Model, Prop, Ref, VModel} from "vue-property-decorator";
import {Validator} from "@/models/validator";
import Deck from "@/models/deck.model";

export type changeAction = (text: string) => void;

 @Options({
     name: 'TextInputField',
     components: { IonInput, IonItem, IonLabel, IonNote },
})
export default class TextInputField extends Vue {

     @Prop( {default:'text'}) type!: string;
     @Prop( {default:'off'}) autocomplete!: string;
     @Prop( {default:''}) helperLabel!: string;
     @Prop( {default:''}) label!: string;
     // eslint-disable-next-line
     @Prop({default: new Validator((text: string) => undefined)})  validators!: Validator[]; // Besser Array, auch von error label
     @Prop() onChange?: changeAction;

     @VModel() errorLabel = '';
     @VModel() content = '';

     public setContent(newContent: string){
         this.content = newContent;
     }
     public getContent(){
         return this.content;
     }

     // Test all Validators
     public validate(): boolean {

         for(let i=0; i < this.validators!.length; i++){

             const message = this.validators[i].validate(this.content);

             if(message !== undefined){
                 this.errorLabel = message;
                 return false;
             }
         }
         this.errorLabel = '';

         return true;
     }

     public onIonChange(event:any): void {
         console.log("Text Input Field onIonChange called ");
         console.log(event.target.value);
         this.content = event.target.value;
         this.validate();
         if(this.onChange !== undefined){
             console.log("onChange callback was defined -> execute");
             this.onChange(this.content);
         }else{
             console.log("onChange callback undefined ");
         }
     }

 }
