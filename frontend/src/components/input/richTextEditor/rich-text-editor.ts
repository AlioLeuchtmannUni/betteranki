import { IonCard, IonGrid, IonRow, IonCol, IonSegment, IonSegmentButton, IonLabel, IonButton} from '@ionic/vue';
import { Options, Vue} from 'vue-class-component';
import {Prop, Ref, VModel} from "vue-property-decorator";
import {Component, DefineComponent} from "vue";
import QuillEditor from "@/components/input/editor/QuillEditor.vue";

const quillEditor = QuillEditor as unknown as DefineComponent;

@Options({
    name: 'RichTextEditor',
    components: { 
        IonCard,
        IonGrid,
        IonRow,
        IonCol,
        IonSegment,
        IonSegmentButton,
        IonLabel,
        quillEditor,
    },
})
export default class RichTextEditor extends Vue {

    @Ref() editor!: typeof quillEditor;

    @Prop({default:''})editorContentGetsPassedHere!: string;

    public editorContent = '<p>Test content bla</p>';

    public beforeMount() {
        this.editorContent = this.editorContentGetsPassedHere;
    }

    public getContent():string {
        return this.editorContent;
    }

    public sync(content:string){
        this.editorContent = content;
    }

    public setContent(content:string){
        (this.editor as typeof quillEditor).setContent(content);
    }
}
