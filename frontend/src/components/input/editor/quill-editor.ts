
import '@vueup/vue-quill/dist/vue-quill.snow.css';
import {Quill} from "@vueup/vue-quill";
import { Options, Vue} from 'vue-class-component';
import {Prop, Ref, VModel, Watch} from "vue-property-decorator";
import {ToolbarTypes} from "@/components/input/editor/toolbar-types";

@Options({
    name: 'QuillEditor',
})
export default class QuillEditor extends Vue  {

    @Ref('quillRef') quillRef!: HTMLDivElement;
    public editor: Quill;

    @VModel() value = '';


    public mounted() {
        console.log('quill mounted ! ');
        this.rebuildEditor();
        this.setValues();
    }

    public beforeUpdate(): void {
        console.log('quill update !');
    }

    public rebuildEditor(): void {
        this.editor = new Quill(
            this.$refs.quillRef,
            {
                modules: {toolbar: ToolbarTypes.toolbarTypes[0],},
                theme: 'snow',
            });
    }

    public setValues():void {
        this.editor.root.innerHTML = this.value;
        this.editor.on('text-change', () => this.update());
    }


    public update():void {
        const content = this.editor.getText() ? this.editor.root.innerHTML : '';
        this.$emit('content-change', content);
    }

    public getContent():string {
        return this.value;
    }

    public setContent(content:string):void {
        this.editor.root.innerHTML = content;
    }

}
