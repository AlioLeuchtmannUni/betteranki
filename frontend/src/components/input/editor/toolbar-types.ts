export class ToolbarTypes {

    static readonly toolbarTypes = [
        [
            [{'header': [1, 2, 3, 4, 5, 6, false]}],
            ['bold', 'italic', 'underline'],
            [{'list': 'ordered'}, {'list': 'bullet'}, {'align': []}],
            ['blockquote', 'code-block', 'link'],
            [{'color': []}, {'background': []}],
            ['link', 'image'],
            [{'align': []}],
        ],
        [
            [{'header': 1}, {'header': 2}],
            ['bold', 'italic', 'underline'],
            [{'list': 'ordered'}, {'list': 'bullet'}, {'align': []}],
        ],
        [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            ['blockquote', 'code-block'],

            [{'header': 1}, {'header': 2}],               // custom button values
            [{'list': 'ordered'}, {'list': 'bullet'}],
            [{'script': 'sub'}, {'script': 'super'}],     // superscript/subscript
            [{'indent': '-1'}, {'indent': '+1'}],         // outdent/indent
            [{'direction': 'rtl'}],                         // text direction

            [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
            [{'header': [1, 2, 3, 4, 5, 6, false]}],

            [{'color': []}, {'background': []}],          // dropdown with defaults from theme
            [{'font': []}],
            [{'align': []}],

            ['link', 'video', 'image'],

            ['clean']                                         // remove formatting button
        ],
        [], // no toolbar
    ];

}