
import {
    IonCard,
    IonCheckbox,
    IonChip,
    IonCol, IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonRow, IonSlide,
    IonSlides
} from '@ionic/vue';
import { Options, Vue} from 'vue-class-component';
import {Prop, VModel} from "vue-property-decorator";
import Deck from '@/models/deck.model';
import './tagList.css';
import Tag from "@/models/tag.model";
import TagService from "@/services/tag.service";

@Options({
    components: {
        IonCheckbox,
        IonCol,
        IonCard,
        IonItem,
        IonLabel,
        IonRow,
        IonIcon,
        IonChip,
        IonList,
        IonSlides,
        IonSlide,
        IonContent,
    },
})

export default class TagList extends Vue {

    @Prop({default:[]}) tags!: Tag[];
    @Prop( {default:false}) deletable!: boolean;

    public tagService: TagService = new TagService();
    private tagColors = ['primary', 'secondary', 'tertiary', 'success', 'warning', 'danger', 'light', 'medium', 'dark',];

    public getTagColor(index:number):string {
        return this.tagColors[index];
    }

    public deleteTag(tag:Tag):void {
        const index = this.tags.indexOf(tag);
        this.tags.splice(index,1);

        if(tag.id === undefined){return;}
        this.tagService.delete(tag);
    }
}
