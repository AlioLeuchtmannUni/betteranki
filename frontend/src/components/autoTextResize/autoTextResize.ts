
import {
    IonCard, IonCardContent, IonCardHeader, IonCardTitle,
    IonCheckbox,
    IonChip,
    IonCol, IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonRow, IonSlide,
    IonSlides
} from '@ionic/vue';
import { Options, Vue} from 'vue-class-component';
import {Prop, VModel} from "vue-property-decorator";
import './autoTextResize.css';


@Options({
    components: {
        IonCheckbox,
        IonCol,
        IonCard,
        IonItem,
        IonLabel,
        IonRow,
        IonIcon,
        IonChip,
        IonList,
        IonSlides,
        IonSlide,
        IonContent,
        IonCardContent,
        IonCardHeader,
        IonCardTitle,
    },
})

export default class AutoTextResize extends Vue {

    @Prop({default: "nothing"}) someText!:string;
    @VModel() dynamicTextSize = this.createFontSizeStyleString(20);

    public mounted(): void{

        const textLength = this.someText!.length;

        if(textLength < 4){
           this.dynamicTextSize = this.createFontSizeStyleString(100);
           return;
        }

        if(textLength < 20){
            this.dynamicTextSize = this.createFontSizeStyleString(20);
            return;
        }

        if(textLength < 50){
            this.dynamicTextSize = this.createFontSizeStyleString(10);
            return;
        }

    }

    private createFontSizeStyleString(size: number): string {

        return `font-size:${size}px`;
    }

}

