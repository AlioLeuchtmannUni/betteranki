
import {
    IonCard,
    IonCheckbox,
    IonChip,
    IonCol, IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonRow,
    IonSlide,
    IonSlides,
    IonPage, IonModal, modalController,
} from '@ionic/vue';
import { Options, Vue} from 'vue-class-component';
import './tagCreationModal.css';
import TagService from "@/services/tag.service";
import TextInputField from "@/components/input/TextInputField/TextInputField.vue";
import {DefineComponent} from "vue";
import Tag from "@/models/tag.model";
import {Prop, Ref, VModel} from "vue-property-decorator";
import {ValidatorFunction} from "@/constants";
import {Validator} from "@/models/validator";


const textInputField = TextInputField as unknown as DefineComponent;

@Options({
    components: {
        IonCheckbox,
        IonCol,
        IonCard,
        IonItem,
        IonLabel,
        IonRow,
        IonIcon,
        IonChip,
        IonList,
        IonSlides,
        IonSlide,
        IonContent,
        IonPage,
        textInputField,
    },
})

export default class TagCreationModal extends Vue {

    public tagService: TagService = new TagService();
    public allTags: Tag[] = [];
    public validators: Validator[] = [Validator.required(),Validator.min(3),Validator.max(200)];
    @VModel() matchingTags: Tag[] = [];
    @Ref('input') input!: typeof textInputField;

    public mounted(): void {
        this.loadAll();
    }

    // für jedes Update suboptimal
    public updated():void {
        this.loadAll();
    }

    public loadAll():void {
        this.tagService.getTags().then(
            (tags) => this.allTags = tags,
            (err) => console.log(err),
        );
    }
    public onConfirmPressed(): void {
        if(!this.input.validate()){return;}
        this.confirm().then();
    }

    public confirm(): Promise<boolean>  {

        const tagName = this.input.getContent();
        const tag = this.allTags.find(tag => tag.name == tagName);

        if(tag === undefined){
            return modalController.dismiss(new Tag(undefined,tagName), 'confirm');
        }else{
            return modalController.dismiss(tag, 'confirm');
        }
    }

    public cancel(): Promise<boolean> {
        return modalController.dismiss(null, 'cancel');
    }

    public updateMatchingTags(text:string): void {
        this.matchingTags = this.allTags.filter(tag => tag.name?.toLowerCase().startsWith(text.toLowerCase()) && tag.name?.toLowerCase() !== text.toLowerCase());
    }
}
