
import {
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonCheckbox,
    IonChip,
    IonCol,
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonPopover,
    IonRow,
    IonSlide,
    IonSlides,
    IonAccordion,
    IonAccordionGroup,
} from '@ionic/vue';
import { Options, Vue} from 'vue-class-component';
import {Prop, VModel} from "vue-property-decorator";
import TagList from "@/components/tagList/tagList.vue";
import {DefineComponent} from "vue";
import { Ref } from 'vue-property-decorator';

const tagList = TagList as unknown as DefineComponent;

@Options({
    components: {
        IonCheckbox,
        IonCol,
        IonCard,
        IonItem,
        IonLabel,
        IonRow,
        IonIcon,
        IonChip,
        IonList,
        IonSlides,
        IonSlide,
        IonContent,
        IonCardHeader,
        IonCardTitle,
        IonCardSubtitle,
        IonCardContent,
        IonPopover,
        IonAccordion,
        IonAccordionGroup,
        tagList,
    },
})

export default class DashBoard extends Vue {

	@Ref("apexChart") apexChart!: ApexChart;

    @VModel() options = {};
    @Prop() series = [{}];

    public updateChart(categories: Array<number>, values: Array<number>): void {
		this.options = {
			chart: {
				id: 'vuechart-example'
			},
			xaxis: {
				categories: categories
			}
		};
		this.series = [{
			name: 'Score',
			data: values
		}];
	}
}

