
import {
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardTitle,
    IonCheckbox,
    IonChip,
    IonCol,
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonRow,
    IonSlide,
    IonSlides,
    IonAccordion,
    IonAccordionGroup,
    IonItemDivider,
} from '@ionic/vue';
import { Options, Vue} from 'vue-class-component';
import {Prop, VModel} from "vue-property-decorator";
import Deck from '@/models/deck.model';
import './cardItem.css';

import TagList from "@/components/tagList/tagList.vue";
import {VNode} from "ionicons/dist/types/stencil.core";
import {DefineComponent} from "vue";
import Tag from "@/models/tag.model";
import Card from "@/models/card.model";
import {goToType, removeType} from "@/constants";
import CardMeta from "@/models/card_meta";
import CardMetaService from "@/services/card_meta.service";

const tagList = TagList as unknown as DefineComponent;

@Options({
    components: {
        IonCheckbox,
        IonCol,
        IonCard,
        IonItem,
        IonLabel,
        IonRow,
        IonIcon,
        IonChip,
        IonList,
        IonSlides,
        IonSlide,
        IonContent,
        IonCardContent,
        IonCardHeader,
        IonCardTitle,
        IonAccordion,
        IonAccordionGroup,
        IonItemDivider,
        tagList,
    },
})

export default class CardItem extends Vue {

    @Prop({default: false}) readOnly!: boolean;
    @Prop({default: undefined}) card!: Card;
    @Prop() goToCardHandler!: goToType;
    @Prop() deleteCardHandler!: removeType;

    public cardMetaDataService: CardMetaService = new CardMetaService();
    public cardMetaData?: CardMeta;

    @VModel() dropDown =  'caret-down-outline';

    public mounted(): void {
        console.log('card mounted!');
        /*
        this.cardMetaDataService
            .getAll()
            .then(
                (res) => {
                    console.log(res.data);
                    this.cardMetaData = res.data;
                }
            );
         */
    }

    public onToggleDropDown(){
        this.dropDown = (this.dropDown === 'caret-down-outline' ? 'caret-up-outline' : 'caret-down-outline');
    }

}

