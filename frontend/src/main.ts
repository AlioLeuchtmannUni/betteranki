import router from './router';
import {setupAxiosInterceptors} from "@/shared/config/axios-interceptor";
import { IonicVue } from '@ionic/vue';
import {createApp, VNode} from 'vue';
import CardService from "@/services/card.service";
import App from './App.vue';

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';
/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css';
import '@ionic/vue/css/float-elements.css';
import '@ionic/vue/css/text-alignment.css';
import '@ionic/vue/css/text-transformation.css';
import '@ionic/vue/css/flex-utils.css';
import '@ionic/vue/css/display.css';

/* Theme variables */
import './theme/variables.css';
import './theme/global.css';
import TokenService from "@/services/token.service";
import ToastService from "@/services/toast.service";

import VueApexCharts from "vue3-apexcharts";


const vue = createApp(App as unknown as VNode);
vue.use(VueApexCharts);
vue.use(IonicVue);
vue.use(router);


router.isReady().then(() => {
    vue.mount('#app');
});


const tokenService = new TokenService();
const toastService = new ToastService();
let i = 0;

router.beforeEach(
    async (to:any, from:any, next:any) => {

        console.log(`before each route called ${i++}`);
        if(to.name == 'not-found' && from.name == 'not-found'){
            console.log('route not found');
            next('/login');
            return;
        }

        const needsAuthentication = to.meta.needsAuthentication;

        if (!to.matched.length) {
            toastService.presentToast("bottom","Page not found");
            next('/home');
        } else if (needsAuthentication) {

            // go to page if authorization returns successfull
            if(await tokenService.isAuthorized()){
                console.log("invoking next");
                console.log(to);
                next();
                //return;
            }else {
                toastService.presentToast("bottom","You have to log in");
                next('/login');
            }

        } else {
            // no authorities needed, so just proceed
            next();
        }
    }
);


setupAxiosInterceptors(
    (res:any) => {
        // Apply Configs, mit auth header
      const url = res.response?.config?.url;
      const status = res.status || res.response.status;
      if (status === 401) {
          console.log('GOT 401 on REQUEST !');
          console.log(res);
      }
      console.log('Unauthorized!');
      return Promise.reject(res);
    },
    (error:any) => {
      console.log('Server error!');
      return Promise.reject(error);
    }
);
