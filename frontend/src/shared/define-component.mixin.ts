import {DefineComponent} from "vue";

export default class DefineComponentMixin {

    public asDefineComponent(): DefineComponent {
        return this as unknown as DefineComponent;
    }

}