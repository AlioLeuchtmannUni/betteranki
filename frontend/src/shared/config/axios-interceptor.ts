import axios from 'axios';
import {SERVER_API_URL} from "@/constants";
import TokenService from "@/services/token.service";


/// Um allen Anfragen automatisch die notwendigen Header zu verpassen

const TIMEOUT = 1000000;
const onRequestSuccess = (config:any) => {

  const token = new TokenService().getToken();

  if (token) {
    if (!config.headers) {
      config.headers = {};
    }
    config.headers.Authorization = `Bearer ${token}`;
  }
  config.timeout = TIMEOUT;
  config.url = `${SERVER_API_URL}${config.url}`;
  return config;
};

const setupAxiosInterceptors = (onUnauthenticated:any, onServerError:any) => {
  const onResponseError = (err:any) => {
    const status = err.status || err.response.status;
    if (status === 403 || status === 401) {
      return onUnauthenticated(err);
    }
    if (status >= 500) {
      return onServerError(err);
    }
    return Promise.reject(err);
  };

  if (axios.interceptors) {
    axios.interceptors.request.use(onRequestSuccess);
    axios.interceptors.response.use((res:any) => res, onResponseError);
  }
};

export { onRequestSuccess, setupAxiosInterceptors };
