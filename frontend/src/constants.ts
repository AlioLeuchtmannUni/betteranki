export const SERVER_API_URL = 'http://localhost:8080/api';
export const APP_NAME = 'Betteranki';

export type ValidatorFunction = (text:string) => string | undefined;
export type goToType = (id: string | undefined) => Promise<void>;
export type removeType = (id: string | undefined) => void;
