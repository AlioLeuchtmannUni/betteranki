import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonGrid, IonCol, IonRow, IonButton } from '@ionic/vue';
import './LearnStatistic.css';
import { Options, Vue } from 'vue-class-component';
import {DefineComponent} from "vue";
import DashBoard from "@/components/dashBoard/dashBoard.vue";

import { Ref } from 'vue-property-decorator';
import StatsService from '@/services/stats.service';
import StatsPair from '@/models/stats.model';
const dashBoard = DashBoard as unknown as DefineComponent;


@Options({
	name: 'HomePage',
	components: {
		IonCol,
		IonContent,
		IonGrid,
		IonHeader,
		IonPage,
		IonRow,
		IonTitle,
		IonToolbar,
		IonButton,
		dashBoard
	}
})


export default class LearnStatistic extends Vue {

	@Ref("cardCountChart") cardCountChart!: typeof dashBoard;
	@Ref("learnedCardsChart") learnedCardsChart!: typeof dashBoard;
	@Ref("scoreChart") scoreChart!: typeof dashBoard;
	
	statsService: StatsService = new StatsService();

	// cut year from dates
	public formatDates(arr: Array<string>): Array<string> {
		for (let i = 0; i < arr.length; i++) {
			arr[i] = arr[i].substring(5, 10);
		}
		return arr;
	}

	public loadAll(): void {
		this.statsService.getCardCount().then(x => {
			this.cardCountChart.updateChart(
				x.data.stats.map((x: StatsPair) => x.descriptor),
				x.data.stats.map((x: StatsPair) => x.value)
			);
		}
		);
		this.statsService.getLearned().then(x => {
			this.learnedCardsChart.updateChart(
				this.formatDates(x.data.stats.map((x: StatsPair) => x.descriptor)),
				x.data.stats.map((x: StatsPair) => x.value)
			);
		}
		);
		this.statsService.getScores().then(x => {
			this.scoreChart.updateChart(
				x.data.stats.map((x: StatsPair) => x.descriptor),
				x.data.stats.map((x: StatsPair) => x.value)
			);
		}
		);
		
	}

	public mounted(): void {
		this.loadAll();
	}

	public updated() {
		this.loadAll();
	}
}
