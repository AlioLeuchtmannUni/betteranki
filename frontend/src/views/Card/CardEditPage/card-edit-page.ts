
import {
    IonContent,
    IonPage,
    IonTitle,
    IonGrid,
    IonCol,
    IonRow,
    IonCard,
    IonCardContent,
    IonInput,
    IonButton,
    IonSegment,
    IonSegmentButton,
    IonLabel,
    IonIcon, IonHeader, IonToolbar,
} from '@ionic/vue';
import './CardEditPage.css'
import { Options, Vue} from 'vue-class-component';
import {DefineComponent} from "vue";
import {Inject, Prop, Ref, VModel} from "vue-property-decorator";
import RichTextEditor from "@/components/input/richTextEditor/RichTextEditor.vue";
import CardService from "@/services/card.service";
import Card from "@/models/card.model";
import ToastService from "@/services/toast.service";
import TextInputField from "@/components/input/TextInputField/TextInputField.vue";
import AlertService from "@/services/alert.service";

const richTextEditor = RichTextEditor as unknown as DefineComponent;
const textInputField = TextInputField as unknown as DefineComponent;

@Options({
    name: 'CardEditPage',
    components: {
        IonToolbar,
        IonHeader,
        IonContent,
        IonPage,
        IonGrid,
        IonCol,
        IonRow,
        IonTitle,
        IonCard,
        IonCardContent,
        IonInput,
        IonButton,
        IonSegment,
        IonSegmentButton,
        IonLabel,
        IonIcon,
        richTextEditor,
        textInputField,
    }
})
export default class CardEditPage extends Vue  {

    @VModel() card: Card = new Card('','','',undefined,undefined)

    @Ref() questionEditor!: typeof richTextEditor;
    @Ref() answerEditor!: typeof richTextEditor;
    @Ref('titleInputField') titleInputField!: typeof textInputField;

    private cardService: CardService = new CardService();
    private toastService: ToastService = new ToastService();

    public questionState = true;

    // When mounted -> Fetch id if id in route != undefine -> Edit mode
    public mounted() {
        const id = this.$route.params.cardId;

        if(id !== undefined){
            this.cardService
                .getCardByID(id as string)
                .then(res => {
                    console.log('card recieved: ');
                    console.log(res.data);
                    this.card = res.data;
                    this.updateForm();
                });
        }

    }

    private updateForm(): void {
        (this.questionEditor as typeof richTextEditor).setContent(this.card.question_html);
        (this.answerEditor as typeof richTextEditor).setContent(this.card.answer_html);
        (this.titleInputField as typeof textInputField).setContent(this.card!.name);

    }


    public changeState(selection:any):void {
        this.questionState = (/true/i).test(selection.detail.value);
    }

    // TODO: erweiterung der Validierung
    public submitForm(){

        // TODO: Labels unter den Input Elementen anstelle von Toasts
        if((this.titleInputField as typeof textInputField).content === ''){
          this.toastService.presentToast('bottom','Title cant be empty !').then();
          return;
        }

        const questionEditorContent = (this.$refs.questionEditor as typeof richTextEditor).getContent();
        if(questionEditorContent === ''){
            this.toastService.presentToast('bottom','Question cant be empty !').then();
            return;
        }

        const answerEditorContent = (this.$refs.answerEditor as typeof richTextEditor).getContent();
        if(answerEditorContent === ''){
            this.toastService.presentToast('bottom','Answer cant be empty !').then();
            return;
        }

        this.save();
    }

    private create(cardToSave: Card): void {

        this.cardService
            .save(cardToSave)
            .then(res=>this.$router.push(`/deck/${this.$route.params.deckId}`),
                err=>{ console.log(err); }
            );
    }

    private update(cardToSave: Card): void {
        this.cardService
            .update(cardToSave)
            .then(
                res => this.$router.push(`/deck/${this.$route.params.deckId}`),
                err => console.log(err),
            );
    }

    private save():void {

        const question = (this.$refs.questionEditor as typeof richTextEditor).getContent();
        const answer = (this.$refs.answerEditor as typeof richTextEditor).getContent();

        // TODO: check id
        console.log('on save i have:');
        console.log(this.card.id);
        console.log(this.$route.params.deckId );

        const cardToSave = new Card(
            this.titleInputField.getContent(),
            question,
            answer,
            this.$route.params.deckId as string,
            this.card.id,
        );

        if(cardToSave.id === undefined){
            this.create(cardToSave);
        }else{
            this.update(cardToSave);
        }
    }

    public onBackPressed():void {
        AlertService.confirmationPopUp(
            "Sure to leave without saving ? ",
            ()=>this.$router.push(`/deck/${this.$route.params.deckId}`),
        ).then();
    }
}
