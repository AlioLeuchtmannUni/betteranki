import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonGrid,
    IonCol,
    IonRow,
    IonButton,
    IonFooter,
    IonCardContent,
    IonCard,
} from '@ionic/vue';
import './LearnPage.css'
import Card from '@/models/card.model';
import DeckService from '@/services/deck.service';
import Deck from '@/models/deck.model';
import { Options, Vue } from 'vue-class-component'
import { Prop, Ref, VModel } from "vue-property-decorator";
import CardMetaService from '@/services/card_meta.service';
import CardMeta from '@/models/card_meta';
import ToastService from "@/services/toast.service";


@Options({
    name: 'HomePage',
    components: {
        IonCol,
        IonContent,
        IonGrid,
        IonHeader,
        IonPage,
        IonRow,
        IonTitle,
        IonToolbar,
        IonButton,
        IonFooter,
        IonCardContent,
        IonCard,
    }
})
export default class LearnPage extends Vue {

    @VModel() cards: Card[] = [];
    @VModel() deck: Deck = new Deck('', '',[], false,[]);
    @VModel() answerVisible = false;
    @VModel() dynamicQuestionSize = "font-size: 20px";
    @VModel() dynamicAnswerSize = "font-size: 20px";

    @VModel() dashboardModus = false;

    @VModel() easyCardsCounter = 0;
    @VModel() goodCardsCounter = 0;
    @VModel() hardCardsCounter = 0;
    @VModel() againCardsCounter = 0;
    private maxCounter = 0;

    private cardMetaService = new CardMetaService();
    private deckService = new DeckService();
    private toastService = new ToastService();

    private metas: CardMeta[] = [];

    @VModel() barStyleAgain = "";
    @VModel() barStyleHard = "";
    @VModel() barStyleGood = "";
    @VModel() barStyleEasy = "";

    public currentCardIndex = 0;

    private setBarChartValues(){
        this.barStyleAgain = this.getBarChartStyle(0);
        this.barStyleHard = this.getBarChartStyle(1);
        this.barStyleGood = this.getBarChartStyle(2);
        this.barStyleEasy = this.getBarChartStyle(3);
        this.dashboardModus=true;
    }

    private getBarChartStyle(index: number): string {
        switch(index){
            case 0: return "width: 40px;  position: absolute; bottom: 0;" + `background-color: red; height:${(this.againCardsCounter/this.maxCounter) * 100}%;`;
            case 1: return "width: 40px;  position: absolute; bottom: 0;" + `background-color: orange; height:${(this.hardCardsCounter/this.maxCounter) * 100}%;`;
            case 2: return "width: 40px;  position: absolute; bottom: 0;" + `background-color: blue; height:${(this.goodCardsCounter/this.maxCounter) * 100}%;`;
            case 3: return "width: 40px;  position: absolute; bottom: 0;" + `background-color: green; height:${(this.easyCardsCounter/this.maxCounter) * 100}%;`;
            default: return "width: 40px;  position: absolute; bottom: 0;" + `height:100%;`;
        }
    }

    // returns font-size based on text length
    public dynamicFontSize(text: string): string {
        if (text.length < 20) {
            return "font-size: 2em";
        } else if (text.length < 40) {
            return "font-size: 2em";
        } else if (text.length < 60) {
            return "font-size: 1.5em";
        } else if (text.length < 80) {
            return "font-size: 1.2em";
        } else {
            return "font-size: 1em";
        }
    }

    public setFontSize(): void {
        this.dynamicQuestionSize = this.dynamicFontSize(this.cards[this.currentCardIndex].question_html);
        this.dynamicAnswerSize = this.dynamicFontSize(this.cards[this.currentCardIndex].answer_html);
    }

    public showResult(): void {
        this.answerVisible = true;
    }

    public nextCard(score: number): void {
        const currentCard = this.cards[this.currentCardIndex];
        const currentCardMeta = new CardMeta(+currentCard.id!, score);
        this.maxCounter++;
        switch(currentCardMeta.score){
            case 0: this.againCardsCounter++; break;
            case 1:  this.hardCardsCounter++; break;
            case 2:  this.goodCardsCounter++; break;
            case 3:  this.easyCardsCounter++; break;
        }

        this.answerVisible = false;
        this.cardMetaService.save(currentCardMeta).then(res => console.log("saved meta data",res));

        // check before incrementation -> sonst in view error weil card undefined !
        if ((this.currentCardIndex + 1) >= this.cards.length) {
            this.setBarChartValues();
            this.toastService.presentToast("bottom","Learning completed");
            return;
        }
        this.currentCardIndex++;
        this.setFontSize();
    }


    public mounted(): void {

        console.log("mounted learn page");
        const deckId = this.$route.params.id as string;
        this.deckService
            .getDeckById(deckId)
            .then(
                (res) => {
                    this.deck = res.data
                    this.cards = res.data.cards;

                    if(this.cards.length == 0 ){
                        this.toastService.presentToast("bottom","No Cards to Learn");
                        this.$router.go(-1);
                        return;
                    }

                    this.setFontSize();
                },
                (err) => console.log(err)
            );
    }


}

