import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonGrid, IonCol, IonRow, IonButton, IonItem, IonText, IonLabel} from '@ionic/vue';
import './HomePage.css'
import { Options, Vue} from 'vue-class-component'
import {DefineComponent} from "vue";

import Deck from "@/models/deck.model";
import DeckService from "@/services/deck.service";
import {VModel} from "vue-property-decorator";
import AlertService from "@/services/alert.service";

import DeckList from "@/components/deckList/deckList.vue";
import ToastService from "@/services/toast.service";
const deckList = DeckList as unknown as DefineComponent;

@Options({
    name: 'HomePage',
    components: {
        IonCol,
        IonContent,
        IonGrid,
        IonHeader,
        IonPage,
        IonRow,
        IonTitle,
        IonToolbar,
        IonButton,
        IonItem,
        IonText,
        IonLabel,
        deckList,
    }
})
export default class HomePage extends Vue {

    @VModel() decks?: Deck[] = [];
    private deckService: DeckService = new DeckService();

    private toastService = new ToastService();

    public mounted() {
        this.loadAll();
    }

    public updated() {
        this.loadAll();
    }

    public onSharePressed(deck: Deck): void{
        if(deck.shared){
            this.deckService
            .unshareDeck(deck.id!)
            .then(
                (res) => {
                    this.toastService.presentToast("bottom","Deck is not longer being shared.");
                    console.log(res);
                    deck.shared = false;
                },
                (err) => {
                    this.toastService.presentToast("bottom","Unsharing failed.");
                    console.log(err);
                }
            );
        }else{
            this.deckService
            .shareDeck(deck.id!)
            .then(
                (res) => {
                    this.toastService.presentToast("bottom","Deck was shared successfully.");
                    console.log(res);
                    deck.shared = true;
                },
                (err) => {
                    this.toastService.presentToast("bottom","Deck could not be shared.");
                    console.log(err);
                }
            );
        }  
    }

    public loadAll(): void {
        this.deckService
            .getAllDecksForUser()
            .then( (res) => {
                    this.decks = res.data.decks.filter( (deck: Deck) => deck.favourite);
                },
                (err) => {
                    console.log(err);
                }
            );
    }


    public onGoToDeckPressed(deck: Deck) {
        this.$router.push(`/deck/${deck.id}`);
    }

    public onLearnDeckPressed(deck: Deck): void{
        this.$router.push(`/learn/${deck.id}`);
    }

    public onDeleteDeckPressed(deck: Deck): void{

        // TODO: schöner
        AlertService.confirmationPopUp(
            `Sure to delete ? Deck: ${deck!.name}
            and its ${deck!.cards?.length} Cards ? `,
            () => {
                this.deckService.delete(deck.id!)
                    .then(
                        (res) => {
                            this.decks = this.decks!.filter((curr) => curr.id !== deck.id);
                        },
                        (err) => {
                            console.log(err);
                        }
                    );
            },
        ).then();
    }

}
