import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonGrid, IonCol, IonRow, IonButton} from '@ionic/vue';

import { Options, Vue} from 'vue-class-component'
import {DefineComponent} from "vue";

import Deck from "@/models/deck.model";
import DeckService from "@/services/deck.service";
import {VModel} from "vue-property-decorator";

import DeckList from "@/components/deckList/deckList.vue";
const deckList = DeckList as unknown as DefineComponent;

@Options({
    name: 'Discover',
    components: {
        IonCol,
        IonContent,
        IonGrid,
        IonHeader,
        IonPage,
        IonRow,
        IonTitle,
        IonToolbar,
        IonButton,
        deckList,
    }
})
export default class DiscoverScreen extends Vue {

    @VModel() decks?: Deck[] = [];
    private deckService: DeckService = new DeckService();

    public mounted() {
        this.loadAll();
    }

    public updated() {
        this.loadAll();
    }

    public onLearnDeckPressed(deck: Deck): void{
        this.$router.push(`/learn/${deck.id}`);
    }

    public onViewDeckPressed(deck: Deck): void{
        this.$router.push(`/deck/${deck.id}/true`);
    }

    public loadAll(): void {
        this.deckService
            .getSharedDecks()
            .then(
                (res) => this.decks = res.data.decks,
                (err) => console.log(err)
            );
    }


}
