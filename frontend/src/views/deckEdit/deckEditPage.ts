
import {
    IonContent,
    IonPage,
    IonTitle,
    IonGrid,
    IonCol,
    IonRow,
    IonCard,
    IonCardContent,
    IonInput,
    IonButton,
    IonSegment,
    IonSegmentButton,
    IonLabel,
    IonIcon,
    IonHeader,
    IonToolbar,
    IonItem,
    IonModal,
    alertController,
    IonAvatar,
    IonCardHeader,
    IonCardTitle, AlertInput, modalController,
} from '@ionic/vue';
import './deckEditPage.css'
import { Options, Vue} from 'vue-class-component';
import {DefineComponent} from "vue";
import ToastService from "@/services/toast.service";
import Deck from "@/models/deck.model";
import Card from "@/models/card.model";
import DeckService from "@/services/deck.service";
import CardService from "@/services/card.service";
import FloatingActionButton from "@/components/layout/floatingActionSheetButton/FloatingActionSheetButton.vue";
import TextInputField from "@/components/input/TextInputField/TextInputField.vue";
import TagList from "@/components/tagList/tagList.vue";
import {Prop, Ref, VModel} from "vue-property-decorator";
import Tag from "@/models/tag.model";
import TagService from "@/services/tag.service";
import AlertService from "@/services/alert.service";
import CardItem from "@/components/cardItem/cardItem.vue";
import TagCreationModal from "@/components/tagCreationModal/tagCreationModal.vue";

const floatingActionButton = FloatingActionButton as unknown as DefineComponent;
const textInputField = TextInputField as unknown as DefineComponent;
const tagList = TagList as unknown as DefineComponent;
const cardItem = CardItem as unknown as DefineComponent;
const tagCreationModal = TagCreationModal as unknown as DefineComponent;

@Options({
    name: 'DeckEditPage',
    components: {
        IonToolbar,
        IonHeader,
        IonContent,
        IonPage,
        IonGrid,
        IonCol,
        IonRow,
        IonTitle,
        IonCard,
        IonCardContent,
        IonInput,
        IonButton,
        IonSegment,
        IonSegmentButton,
        IonLabel,
        IonIcon,
        IonItem,
        IonModal,
        IonAvatar,
        IonCardHeader,
        IonCardTitle,
        floatingActionButton,
        textInputField,
        tagList,
        cardItem,
        tagCreationModal,
    }
})
export default class DeckEditPage extends Vue  {

    @Prop( {default:()=>console.log('leaving deck edit')}) leaveCallback!: VoidFunction;

    @Ref('deckTitle') deckTitle?: typeof textInputField;
    @Ref('tagCreationModal') tagCreationModal?: any;

    @VModel() deck: Deck = new Deck('testTitle', 'test Description',[], false,[], );
    @VModel() cards: Card[] = [];
    @VModel() tags: Tag[] = [];
    @VModel() readOnly = false;

    private deckService: DeckService = new DeckService();
    private cardService: CardService = new CardService();
    private toastService: ToastService = new ToastService();
    private tagService: TagService = new TagService();


    // When mounted -> Fetch id if id in route != undefine -> Edit mode
    public mounted(): void {
        (this.deckTitle as typeof textInputField).setContent("");
        this.loadAll();

        const r = this.$route.params.readOnly as string;
        if(r !== '' && r !== undefined){
            if(r === 'true'){
                this.readOnly=true;
            }else {
                this.readOnly=false;
            }
        }
    }

    // für jedes Update suboptimal
    public beforeUpdate():void {
        this.loadAll();
    }

    public loadAll(): void {
        this.tagService.getTags()
            .then(
                (res) => this.tags = res,
                (err) => console.log(err)
            );

        const id = this.$route.params.id as string;
        if(id === '' || id === undefined){return;}
        this.loadDeckData(id);
    }

    public loadDeckData(id: string): void {
        this.deckService
            .getDeckById(id)
            .then(
                (res) => {
                    this.deck = res.data;
                    this.cardService
                        .getAllCardsByDeck(this.$route.params.id as string)
                        .then(
                            (res) => {
                                this.deck!.cards = res.data;
                                this.setValues(this.deck);
                            },
                            (err) => console.log(err),
                        );
                },
                (err) => console.log(err),
            );
    }

    public setValues(deck: Deck):void {
        this.cards = deck!.cards ?? [];
        if(this.readOnly){return;}
        (this.deckTitle as typeof textInputField).setContent(deck.name!);
    }


    public async onGoToCardPressed(id: string | undefined): Promise<void> {
        let created = true;
        if(this.deck!.id == undefined){
            created = await this.save();
        }

        if (created) {
            this.$router.push(`/deck/${this.deck?.id}/card/${id}`);
        }

    }


    private removeCard(cardId: string | undefined): void{
        if(this.deck.id !== undefined && cardId !== undefined){
            this.cardService.delete(cardId!)
                .then(
                    (res)=>console.log(res),
                    (err)=>console.log(err),
                );
        }

        // remove from RAM
        this.cards = this.cards.filter((card) => card.id !== cardId );
    }

    public onRemoveCardPressed(cardId: string | undefined): void{
        AlertService.confirmationPopUp(
            "Sure to delete Card ?",
            ()=>{this.removeCard(cardId)},)
            .then();
    }

    public onGoBackPressed(){

        if(this.readOnly){
            this.$router.go(-1);
            return;
        }

        AlertService.confirmationPopUp(
            "Sure to leave without saving ? ",
            ()=>this.$router.go(-1),
        ).then();
    }

    public update():Promise<boolean>{
        return new Promise<any>((resolve, reject) => {
            this.deckService
                .update(this.deck)
                .then(res=>{
                        console.log(res);
                        this.toastService.presentToast("bottom", "updated").then();
                        this.$router.push('/decks');
                        resolve(true);
                    },
                    (err)=>{
                        console.log(err);
                        this.toastService.presentToast("bottom", "could not be updated").then();
                        resolve(false);
                    }
                );
        });
    }

    public create(): Promise<boolean> {
        return new Promise<any>((resolve, reject) => {
            this.deckService
                .save(this.deck)
                .then(res=>{
                        console.log(res);
                        this.toastService.presentToast("bottom","saved").then();
                        this.$router.push('/decks');
                        resolve(true);
                    },
                    (err)=>{
                        console.log(err);
                        this.toastService.presentToast("bottom","could not be saved").then();
                        resolve(false);
                    }
                );
        });
    }

    public async save():Promise<boolean>{
        this.deck!.name = (this.deckTitle as typeof textInputField).content;

        if((this.deckTitle as typeof textInputField).content === ''){
            this.toastService.presentToast('bottom','Title cant be empty !').then();
            return false;
        }

        if(this.deck.id !== undefined) {
            return this.update();
        }

        return this.create();
    }

    public async openTagCreationModal(): Promise<void> {
        const modal = await modalController.create({component: tagCreationModal,});
        await modal.present();

        const {data, role} = await modal.onWillDismiss();
        if(role === 'confirm'){
            const newTag = data as Tag;
            const indexOfSametag = this.deck.tags.findIndex(tag => tag.name === newTag.name);
            if(indexOfSametag >= 0){
                return;
            }else {
                this.deck.tags.push(newTag);
            }
        }
    }



}
