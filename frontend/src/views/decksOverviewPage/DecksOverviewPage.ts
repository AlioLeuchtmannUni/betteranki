import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonGrid,
    IonCol,
    IonRow,
    IonButton,
    IonIcon,
    IonCard,
    IonLabel,
    IonItem,
    IonText,
    alertController,
    AlertInput
} from '@ionic/vue';
import './DecksOverviewPage.css'
import { Options, Vue} from 'vue-class-component'
import DeckService from '@/services/deck.service';
import Deck from '@/models/deck.model';
import {DefineComponent} from "vue";

import FloatingActionSheetButton from "@/components/layout/floatingActionSheetButton/FloatingActionSheetButton.vue";
import ActionButton from "@/models/actionButton";
const floatingActionSheetButton = FloatingActionSheetButton as unknown as DefineComponent;
import ToastService from "@/services/toast.service";
import Tag from "@/models/tag.model";
import TagService from "@/services/tag.service";
import {VModel} from "vue-property-decorator";
import AlertService from "@/services/alert.service";

import DeckItem from '@/components/deckItem/deck-item.vue';
const deckItem = DeckItem as unknown as DefineComponent;

import DeckList from '@/components/deckList/deckList.vue';
const deckList = DeckList as unknown as DefineComponent;

@Options({
    name: 'HomePage',
    components: {
        IonCol,
        IonContent,
        IonGrid,
        IonHeader,
        IonPage,
        IonRow,
        IonTitle,
        IonToolbar,
        IonButton,
        IonIcon,
        IonCard,
        IonLabel,
        IonItem,
        IonText,
        floatingActionSheetButton,
        deckItem,
        deckList,
    }
})
export default class DecksOverviewPage extends Vue {

    @VModel() isLoading = false;

    public deckService: DeckService = new DeckService();
    public toastService: ToastService = new ToastService();
    public tagService: TagService = new TagService();

    @VModel() filter: string[] = [];
    @VModel() displayedDecks: Deck[] = [];
    @VModel() decks: Deck[] = [];
    @VModel() mySharedDecks: Deck[] = [];
    public tags?: Tag[] = [];

    public beforeMount(): void {
       this.loadAll().then((res)=>{console.log('done loading');})
    }

    public beforeUpdate(): void {
        this.loadAll().then((res)=>{console.log('done loading');})
    }

    public async loadAll(): Promise<void> {
        this.isLoading = true;
        await this.loadDecks();
        await this.loadTags();
        this.isLoading = false;
    }

    public loadTags(): void {
        this.tagService
            .getTags()
            .then(
                (tags) => this.tags = tags,
                (err) => console.log(err),
            );

    }

    public reload(): void {
        this.filter = [];
        this.loadAll().then();
    }

    public loadDecks(): void {
        this.deckService
            .getAllDecksForUser()
            .then(
                (res) => {
                    this.decks = res.data.decks;
                    this.displayedDecks = this.decks;
                },
                (err) => {
                    this.toastService.presentToast("middle","Error loading Decks from server: " + err);
                }
            );

        this.deckService.getMySharedDecks().then(
            (res) => {
                console.log('shared decks');
                console.log(res);
                this.mySharedDecks = res.data.decks
            },
            (err) => console.log(err),
        );


    }

    public onGoToDeckPressed(deck: Deck) {
        this.$router.push(`/deck/${deck.id}`);
    }

    public onLearnDeckPressed(deck: Deck): void{
        this.$router.push(`/learn/${deck.id}`);
    }

    public showOnlyShard(): void {
        //this.displayedDecks = this.decks.filter()
        console.log("");
    }

    public onSharePressed(deck: Deck): void{
        if(deck.shared){
            this.deckService
            .unshareDeck(deck.id!)
            .then(
                (res) => {
                    this.toastService.presentToast("bottom","Deck is not longer being shared.");
                    console.log(res);
                    deck.shared = false;
                },
                (err) => {
                    this.toastService.presentToast("bottom","Unsharing failed.");
                    console.log(err);
                }
            );
        }else{
            this.deckService
            .shareDeck(deck.id!)
            .then(
                (res) => {
                    this.toastService.presentToast("bottom","Deck was shared successfully.");
                    console.log(res);
                    deck.shared = true;
                },
                (err) => {
                    this.toastService.presentToast("bottom","Deck could not be shared.");
                    console.log(err);
                }
            );
        }  
    }


    public onDeleteDeckPressed(deck: Deck): void{

        AlertService.confirmationPopUp(
            `Sure to delete ? Deck: ${deck!.name}
            and its ${deck!.cards?.length} Cards ? `,
            () => {

                this.isLoading = true;
                this.deckService.delete(deck.id!)
                    .then(
                        (res) => {
                            this.decks = this.decks.filter((curr) => curr.id !== deck.id);
                            this.displayedDecks = this.displayedDecks.filter((curr) => curr.id !== deck.id);
                            this.isLoading = false;
                        },
                        (err) => {
                            console.log(err);
                            this.isLoading = false;
                        }
                    );
            },
        ).then();
    }

    public onAddClicked(): void {
        this.$router.push('/deck/');
    }

    // TODO: OPTIONAL backend Funktion für suche -> da bei decks später paging angewandt wird
    public search(): void {
        this.createSearchFieldPopUp().then();
    }

    // TODO: OPTIONAL backend Funktion für tag Filtern - da bei decks später paging angewandt wird
    public filterTags(): void {
        this.createTagChoicePopUp().then();
    }


    // TODO: pop ups wenn möglich auslagern
    public async createSearchFieldPopUp(): Promise<void> {

        const alert = await alertController.create({
            header: 'Search',
            buttons: [
                {
                    text: 'SEARCH',
                    handler: (alertData) => {
                        const searchValue = alertData.name;
                        this.filter.push('Starting with ' + searchValue);
                        this.displayedDecks = this.displayedDecks.filter(deck => deck.name.startsWith(searchValue));
                        alert.dismiss();
                    }
                },
                {
                    text: 'CANCEL',
                    handler: () => {
                        alert.dismiss();
                    }
                },
            ],
            inputs: [
                {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Search',
                    attributes: {
                        minLength: 1,
                        maxLength: 50,
                    },
                },
            ],
        });
        return alert.present();
    }

    private async createTagChoicePopUp(): Promise<void> {

        const tagCheckboxes: AlertInput[] = await this.tagService.createAlerts();
        let alert: HTMLIonAlertElement | undefined = undefined;

        alert = await alertController.create({
            header: 'Filter for Tags',
            buttons: [
                {
                    text: 'APPLY',
                    handler: (alertData) => {
                        const chosenTags: Tag[] = alertData;
                        this.displayedDecks = this.displayedDecks.filter( deck => this.containsTag(deck.tags, chosenTags));
                        const names = chosenTags.map( (tag) => tag!.name! );
                        this.filter.push("Selected Tags: " + names);
                        alert!.dismiss();
                    }
                },
                {
                    text: 'CANCEL',
                    handler: () => {
                        alert!.dismiss();
                    }
                },
            ],
            inputs: tagCheckboxes,
        });

        return alert!.present();
    }


    private containsTag(a: Tag[], b: Tag[]): boolean {
        for(let i = 0; i < a.length; i++){
            for(let j = 0; j < b.length; j++){
                if(a[i].id === b[j].id){return true;}
            }
        }
        return false;

    }


}
