

import axios from 'axios';
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonGrid,
    IonCol,
    IonRow,
    IonCard,
    IonCardContent,
    IonMenu,
    IonText,
    IonButton,
    IonIcon,
    IonLabel,
    IonCheckbox,
    IonItem,
    IonFooter,
    alertController,
    AlertInput
} from '@ionic/vue';

import { Options, Vue} from 'vue-class-component';
import {DefineComponent} from "vue";
import {Ref, VModel} from "vue-property-decorator";
import Login from "@/models/login.model";
import AccountService from "@/services/account.service";
import {Validator} from "@/models/validator";
import TokenService from "@/services/token.service";
import MainMenu from "@/components/layout/menu/main-menu.vue";
import TextInputField from "@/components/input/TextInputField/TextInputField.vue";
import ToastService from "@/services/toast.service";
import User from "@/models/user.model";
import AlertService from "@/services/alert.service";

const mainMenu = MainMenu as unknown as DefineComponent;
const textInputField = TextInputField as unknown as DefineComponent;

@Options({
    name: 'LoginPage',
    components: {
        IonLabel,
        IonCard,
        IonCardContent,
        IonCol,
        IonContent,
        IonGrid,
        IonHeader,
        IonPage,
        IonText,
        IonRow,
        IonTitle,
        IonToolbar,
        IonButton,
        IonIcon,
        IonItem,
        IonCheckbox,
        IonFooter,
        textInputField,
        mainMenu,
    }
})
export default class LoginPage extends Vue{

    @Ref('loginRef') loginTextField!: typeof textInputField;
    @Ref('pwRef') passwordTextField!: typeof textInputField;

    @VModel() isLoggedIn = false;
    @VModel() user: User = new User("Default-User", "email@example.com");

    public rememberMe = false;
    public accountDataLoaded = false;

    public loginValidators = [Validator.required(),Validator.min(5),Validator.max(200)];
    public pwValidators = [Validator.required(),Validator.min(5),Validator.max(200)];

    private accountService = new AccountService();
    public tokenService = new TokenService();
    private toastService = new ToastService();

    public mounted():void {
        this.isLoggedIn = this.tokenService.getToken() !== null;
        this.getAccountData();
    }


    public beforeUpdate():void {
        this.isLoggedIn = this.tokenService.getToken() !== null;
        if(this.isLoggedIn){
            this.getAccountData();
        }
    }

    public getAccountData(){
        this.accountService
        .get()
        .then(
            (res) => {
                this.user.username = res.username;
                this.user.email = res.email;
                this.accountDataLoaded = true;
            },
            (err) => {
                console.log(err);
            }
        );
    }

    public onloginClicked(): void {

        // Return if Form is invalid
        if(!this.formValid()){return;}

        const pw = (this.passwordTextField as typeof textInputField).content;
        const username = (this.loginTextField as typeof textInputField).content;
        const login = new Login(pw,username);

        this.accountService.login(login,this.rememberMe)
            .then(success => {
                if(success){
                    this.$router.push('/');
                }else {
                    this.toastService.presentToast("bottom","Wrong Username or Password");
                }
            });
    }

    public formValid(): boolean {

        const loginValid  = (this.loginTextField as typeof textInputField).validate();
        const passwordValid  = (this.passwordTextField as typeof textInputField).validate();

        if(!loginValid){return false;}
        if(!passwordValid) {return false;}

        return true;
    }

    public logout(): void {
        this.accountService.logout();
        this.$router.push('/');
        this.isLoggedIn = false;
        this.accountDataLoaded = false;
    }



    public deleteAccount(){

        AlertService.confirmationPopUp(
            "Delete your Account?",
            () => {
                this.accountService
                .delete()
                .then(
                    (res) => {
                        if(res.success){
                            this.isLoggedIn = false;
                            this.toastService.presentToast("bottom","Account successfully deleted");
                        }
                    },
                    (err) => {
                        console.log(err);
                    }
                );
            },
            undefined,
            `Your decks and cards will be irrevocably deleted.
            Your shared decks stay public unless you manually unshare them
            before deleting your account.` 
        ).then();
    }
}
