

import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonGrid,
    IonCol,
    IonRow,
    IonCard,
    IonCardContent,
    IonMenu,
    IonButton,
    IonIcon,
    IonLabel,
    IonCheckbox
} from '@ionic/vue';

import { Options, Vue} from 'vue-class-component';
import TextInputField from "@/components/input/TextInputField/TextInputField.vue";
import {DefineComponent} from "vue";
import {Ref} from "vue-property-decorator";
import Login from "@/models/login.model";
import AccountService, {REGISTER_SUCCESS_MESSAGE} from "@/services/account.service";
import {Validator} from "@/models/validator";
import Register from "@/models/register.model";
import ToastService from "@/services/toast.service";

const textInputField = TextInputField as unknown as DefineComponent;

@Options({
    name: 'RegisterPage',
    components: {
        IonLabel,
        IonCard,
        IonCardContent,
        IonCol,
        IonContent,
        IonGrid,
        IonHeader,
        IonPage,
        IonRow,
        IonTitle,
        IonToolbar,
        IonButton,
        IonIcon,
        textInputField,
    }
})
export default class RegisterPage extends Vue{

    @Ref('loginRef') loginTextField!: typeof textInputField;
    @Ref('pwRef') passwordTextField!: typeof textInputField;
    @Ref('pwRepeatRef') passwordRepeatTextField!: typeof textInputField;
    @Ref('emailRef') emailTextField!: typeof textInputField;

    public loginValidators = [Validator.required(),Validator.min(5),Validator.max(200)];
    public pwValidators = [Validator.required(),Validator.min(5),Validator.max(200)];
    public pwRepeatValidators = this.pwValidators;
    public emailValidators = [Validator.required(),Validator.min(5),Validator.max(200)];

    private accountService = new AccountService();

    private toastService = new ToastService();


    public register(): void {

        if(!this.formValid()){
            return;
        }

        const pw = (this.passwordTextField as typeof textInputField).content;
        const username = (this.loginTextField as typeof textInputField).content;
        const email = (this.emailTextField as typeof textInputField).content;
        const register = new Register(username,email,pw);

        this.accountService.register(register)
            .then(resultMessage=>{
                    this.toastService.presentToast("bottom",resultMessage);

                    if(resultMessage === REGISTER_SUCCESS_MESSAGE){
                        this.$router.push('/login');
                    }
                });

    }


    public formValid(): boolean {

        const loginValid  = (this.loginTextField as typeof textInputField).validate();
        const passwordValid  = (this.passwordTextField as typeof textInputField).validate();
        const passwordRepeatValid  = (this.passwordRepeatTextField as typeof textInputField).validate();
        const emailValid  = (this.emailTextField as typeof textInputField).validate();

        if(!loginValid){return false;}

        if(!passwordValid) {return false;}

        if(!passwordRepeatValid){return false;}

        if(!emailValid) {return false;}

        if( (this.passwordRepeatTextField as typeof textInputField).content !== (this.passwordTextField as typeof textInputField).content){
            console.log(' Password repeat ungleich ');
            // TODO: -> Toast message
            return false;
        }


        return true;
    }
}
