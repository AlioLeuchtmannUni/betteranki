import { createRouter, createWebHistory } from '@ionic/vue-router';

import HomePage from "@/views/homePage/HomePage.vue";
import LoginPage from "@/views/loginPage/LoginPage.vue";
import DecksOverviewPage from "@/views/decksOverviewPage/DecksOverviewPage.vue";
import CardEditPage from "@/views/Card/CardEditPage/CardEditPage.vue";
import {RouteRecordRaw} from "vue-router";
import {VNode} from "vue";
import RegisterPage from "@/views/registerPage/RegisterPage.vue";
import DeckEditPage from "@/views/deckEdit/deckEditPage.vue";
import LearnPage from "@/views/learnPage/LearnPage.vue"
import LearnStatistic from "@/views/learnStatistic/LearnStatistic.vue"
import DiscoverScreen from "@/views/discover/DiscoverScreen.vue";


const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/home',
    name: 'Home',
    component: HomePage as unknown as VNode,
    meta: {
      needsAuthentication: true,
      checkOwnership: false,
    }
  },
  {
    path: '/decks',
    name: 'Decks',
    component: DecksOverviewPage as unknown as VNode,
    meta: {
      needsAuthentication: true,
      checkOwnership: false,
    }
  },
    /*
    *   {
    path: '/shared',
    name: 'Shared',
    component: DecksOverviewPage as unknown as VNode,
    meta: {
      needsAuthentication: true,
      checkOwnership: false,
    }
  },*/
  {
    path: '/deck/:id?/:readOnly?',
    name: 'Cards',
    component: DeckEditPage as unknown as VNode,
    meta: {
      needsAuthentication: true,
      checkOwnership: true,
    }
  },
  {
    path: '/login',
    name: 'LoginPage',
    component: LoginPage as unknown as VNode,
    meta: {
      needsAuthentication: false,
      checkOwnership: false,
    }
  },
  {
    path: '/register',
    name: 'RegisterPage',
    component: RegisterPage as unknown as VNode,
    meta: {
      needsAuthentication: false,
      checkOwnership: false,
    }
  },
  {
    path: '/deck/:deckId/card/:cardId?',
    name: 'CardEditPage',
    component: CardEditPage as unknown as VNode,
    meta: {
      needsAuthentication: true,
      userIsOwner: ()=>{console.log('get id from card and check owner');}
    }
  },
  {
    path: '/learn/:id',
    name: 'LearnPage',
    component: LearnPage as unknown as VNode,
    meta: {
      needsAuthentication: true
      ,
    }
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: LearnStatistic as unknown as VNode,
    meta: {
      needsAuthentication: true,
    },
  },
  {
    path: '/discover',
    name: 'Discover',
    component: DiscoverScreen as unknown as VNode,
    meta: {
      needsAuthentication: true,
    }
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});


export default router