import CardMeta from "@/models/card_meta";
import axios from "axios";


export default class CardMetaService {

    public save(cardMeta: CardMeta): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            axios
                .post(`/metas`, cardMeta)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

    public get(id: string | undefined): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            axios
                .get(`/metas/${id}`)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

    public getAll(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            axios
                .get(`/metas`)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }




}
