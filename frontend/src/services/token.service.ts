import axios from "axios";

export const TOKEN_STORAGE_IDENTIFIER = 'authenticationToken';
export const REFRESH_TOKEN_IDENTIFIER = 'refreshToken';

export default class TokenService extends EventTarget {
    /// try to get token from local storage
    public getToken(): string | null {

        let token = sessionStorage.getItem(TOKEN_STORAGE_IDENTIFIER)

        if(token === null){
            token = localStorage.getItem(TOKEN_STORAGE_IDENTIFIER);
        }

        return token;
    }


    /// refresh the token
    public refreshToken(): Promise<boolean> {

        const refresh = localStorage.getItem(REFRESH_TOKEN_IDENTIFIER);

        if(refresh != null){

            return new Promise<any>((resolve, reject) => {
                axios
                    .post(`/token/refresh`, { refresh: refresh })
                    .then((res:any) => {
                        console.log(res);

                        sessionStorage.setItem(TOKEN_STORAGE_IDENTIFIER,res.access);
                        sessionStorage.setItem(REFRESH_TOKEN_IDENTIFIER,res.refresh);

                        localStorage.setItem(TOKEN_STORAGE_IDENTIFIER,res.access);
                        localStorage.setItem(REFRESH_TOKEN_IDENTIFIER,res.refresh);

                        resolve(true);
                    },err=>{
                        console.log(err);
                        resolve(false);
                    });
            });
        }

        return new Promise<any>((resolve, reject) => {resolve(false);});

    }


    public checkAccessToken(): Promise<boolean> {


        return new Promise<any>((resolve, reject) => {
            axios
                .post(`/token/verify`, { token: this.getToken() }, )
                .then((res:any) => {
                    console.log(res);
                    resolve(true);
                },err=>{
                    console.log(err);
                    resolve(false);
                });
        });
    }


    isAuthorized(): Promise<boolean> {

        return new Promise<any>((resolve, reject) => {
            this.checkAccessToken()
                .then(tokenValid => {

                        if (tokenValid) {
                            resolve(true);
                        }

                        this.refreshToken().then(refreshed=>{

                                if(refreshed){
                                    resolve(true);
                                }
                                resolve(false);
                            },
                            (err)=>{resolve(false);})
                    },
                    (err)=>{resolve(false);}
                );
        });

    }

    clear(): void {
        sessionStorage.removeItem(TOKEN_STORAGE_IDENTIFIER);
        sessionStorage.removeItem(REFRESH_TOKEN_IDENTIFIER);
        localStorage.removeItem(TOKEN_STORAGE_IDENTIFIER);
        localStorage.removeItem(REFRESH_TOKEN_IDENTIFIER);
    }

}