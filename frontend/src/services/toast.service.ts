import {toastController} from "@ionic/vue";


export default class ToastService {

    public static duration = 1500;

    public async presentToast(position: 'top' | 'middle' | 'bottom', message: string) {
        const toast = await toastController.create({
            message: message,
            duration: ToastService.duration,
            position: position,
        });

        await toast.present();
    }
}