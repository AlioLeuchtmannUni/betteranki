import axios from "axios";
import Card from "@/models/card.model";
import Deck from "@/models/deck.model";


export default class CardService {

  public getAllCardsByDeck(deck_id:string): Promise<any> {

    return new Promise<any>((resolve, reject) => {
      axios
        .get(`/decks/${deck_id}/cards`)
        .then((res:any) => {resolve(res);})
        .catch((err:any) => {reject(err);});
    });
  }

  // Beispiel fetch card per get request unter serverUrl/api/users/:id
  public getCardByID(id:string): Promise<any> {

    return new Promise<any>((resolve, reject) => {
      axios
        .get(`/cards/${id}`)
        .then((res:any) => {resolve(res);})
        .catch((err:any) => {reject(err);});
    });
  }

  public save(card:Card): Promise<any> {

    return new Promise<any>((resolve, reject) => {
      axios
          .post(`/decks/${card.deck_id}/cards`,card)
          .then((res:any) => {resolve(res);})
          .catch((err:any) => {reject(err);});
    });
  }


  public update(card:Card): Promise<any> {

    return new Promise<any>((resolve, reject) => {
      axios
          .put(`/cards/${card.deck_id}`,card)
          .then((res:any) => {resolve(res);})
          .catch((err:any) => {reject(err);});
    });
  }

  public delete(id: string): Promise<any> {

    return new Promise<any>((resolve, reject) => {
      axios
          .delete(`/cards/${id}`)
          .then((res: any) => { resolve(res); })
          .catch((err: any) => { reject(err); });
    });
  }
}
