import {alertController} from "@ionic/vue";


// TODO: alert schöneres design
export default class AlertService {

    public static async confirmationPopUp(header:string, confirmCallback:VoidFunction, cancelCallback?:VoidFunction, message?:string): Promise<void> {

        const alert = await alertController.create({
            header: header,
            message: message,
            buttons: [
                {
                    text: 'CANCEL',
                    handler: () => {
                        if(cancelCallback !== undefined){
                            cancelCallback();
                        }
                        alert.dismiss(false);
                    }
                },
                {
                    text: 'YES',
                    handler: () => {
                        confirmCallback();
                        alert.dismiss(true);
                    }
                },
                
            ],
        });



        return alert.present();

    }
}