import axios, {AxiosRequestConfig} from "axios";
import Login from "@/models/login.model";
import User from "@/models/user.model";
import Register from "@/models/register.model";
import TokenService, {REFRESH_TOKEN_IDENTIFIER, TOKEN_STORAGE_IDENTIFIER} from "@/services/token.service";

export const REGISTER_SUCCESS_MESSAGE = 'Account erfolgreich erstellt';

export default class AccountService {

    public static loggedInStatus: EventTarget = new EventTarget();
    private tokenService = new TokenService();

    public logout(): void {
        this.tokenService.clear();
        AccountService.loggedInStatus.dispatchEvent(new CustomEvent('update', {detail: false }));
    }

    public login(login:Login, rememberMe: boolean): Promise<any> {

        return new Promise<any>((resolve, reject) => {
        axios.
            post('/token/pair', login,)
                .then(result => {

                    console.log(result);
                    const access = result.data.access;
                    const refresh = result.data.refresh;

                    if(rememberMe){
                        sessionStorage.setItem(TOKEN_STORAGE_IDENTIFIER, access);
                        localStorage.setItem(TOKEN_STORAGE_IDENTIFIER, access);

                        sessionStorage.setItem(REFRESH_TOKEN_IDENTIFIER, refresh);
                        localStorage.setItem(REFRESH_TOKEN_IDENTIFIER, refresh);
                    }else {
                        sessionStorage.setItem(TOKEN_STORAGE_IDENTIFIER, access);
                        localStorage.removeItem(TOKEN_STORAGE_IDENTIFIER);

                        sessionStorage.setItem(REFRESH_TOKEN_IDENTIFIER, refresh);
                        localStorage.removeItem(TOKEN_STORAGE_IDENTIFIER);
                    }

                    AccountService.loggedInStatus.dispatchEvent(new CustomEvent('update', {detail: true }));
                    resolve(true);

                }, err => {
                    console.log(err);
                    resolve(false);
                })
                .catch(() => {
                    console.log('error');
                    resolve(false);
                });
        });
    }


    public register(register: Register): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            axios
                .post(`/users/register`, register)
                .then((res:any) => {
                    console.log(res);
                    resolve(REGISTER_SUCCESS_MESSAGE);
                })
                .catch((err:any) => {
                    console.log(err);
                    if(err.response?.data?.detail !== undefined){
                        resolve(err.response.data.detail);
                    }else {
                        return resolve('Unerwarteter Fehler, prüfen Sie ihre Internetverbindung oder wenden Sie sich an den Support.');
                    }
                });
        });
    }

    public get(): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            axios
                .get(`/users`)
                .then((res: any) => { resolve(res.data); })
                .catch((err: any) => { reject(err); });
        });
    }

    public delete(): Promise<any>{
        return new Promise<any>((resolve, reject) => {
            axios
                .delete(`/users`)
                .then((res: any) => { resolve(res.data); })
                .catch((err: any) => { reject(err); });
        });
    }

}
