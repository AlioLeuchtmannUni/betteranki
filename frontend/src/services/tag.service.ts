import axios from "axios";
import Tag from "@/models/tag.model";
import {AlertInput} from "@ionic/vue";


export default class TagService {


    public getTags(): Promise<Tag[]> {

        return new Promise<any>((resolve, reject) => {
            axios
                .get(`/tags`)
                .then((res: any) => { resolve(res.data.tags); })
                .catch((err: any) => { reject(err); });
        });
    }

    public save(tag: Tag): Promise<Tag> {

        return new Promise<any>((resolve, reject) => {
            axios
                .post(`/tags/`, tag)
                .then((res: any) => { resolve(res.data); })
                .catch((err: any) => { reject(err); });
        });
    }

    public update(tag: Tag): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            axios
                .put(`/tags/${tag.id}`, tag)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }


    public delete(tag: Tag): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            axios
                .delete(`/tags/${tag.id}`)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }


    public createAlerts(): Promise<AlertInput[]> {

        return new Promise<any>((resolve, reject) => {
            this.getTags()
                .then((tags: Tag[]) => {

                    const alerts: Array<AlertInput> = [];

                    for(let i = 0; i < tags.length; i++){
                        const tag = tags[i];
                        alerts.push({
                            label: tag.name,
                            type: 'checkbox',
                            value: tag,
                        });
                    }

                    resolve(alerts);

                })
                .catch((err: any) => { reject(err); });
        });

    }
}
