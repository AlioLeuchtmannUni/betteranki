import axios from "axios";
import Deck from "@/models/deck.model";


export default class StatsService {

    public getCardCount(): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            axios
                .get(`/stats/card-count`)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

    public getLearned(): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            axios
                .get(`/stats/learned`)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

    public getScores(): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            axios
                .get(`/stats/scores`)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

}
