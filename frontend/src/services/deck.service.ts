import axios from "axios";
import Deck from "@/models/deck.model";


export default class DeckService {

    public getAllDecksForUser(): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            axios
                .get(`/decks`)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

    public getDeckById(id: string): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            axios
                .get(`/decks/${id}`)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

    public getSharedDecks(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            axios
                .get(`/decks/shared`)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

    public getCopyOfSharedDeck(id: number): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            axios
                .get(`/decks/shared/${id}/copy`)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

    public shareDeck(id: number): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            axios
                .put(`/decks/share/${id}`,'{"shared": true}')
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

    public unshareDeck(id: number): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            axios
                .put(`/decks/share/${id}`,'{"shared": false}')
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

    public getMySharedDecks(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            axios
                .get(`/decks/shared/my`)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

    public delete(id: number): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            axios
                .delete(`/decks/${id}`)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }

    public save(deck: Deck): Promise<any> {

        const deckToSend = JSON.stringify(new Deck(deck.name,deck.description,deck.tags,deck.favourite));
        console.log('try create');
        console.log(deckToSend);

        return new Promise<any>((resolve, reject) => {
            axios
                .post(`/decks`, deckToSend)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }


    public update(deck: Deck): Promise<any> {

        const deckToSend = JSON.stringify(new Deck(deck.name,deck.description,deck.tags,deck.favourite));
        console.log('try update');
        console.log(deckToSend);

        return new Promise<any>((resolve, reject) => {
            axios
                .put(`/decks/${deck.id!}`, deckToSend)
                .then((res: any) => { resolve(res); })
                .catch((err: any) => { reject(err); });
        });
    }
}
