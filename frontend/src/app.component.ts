import {
    IonApp,
    IonRouterOutlet,
    IonPage,
} from '@ionic/vue';
import { Options, Vue} from 'vue-class-component';
import MainMenu from './components/layout/menu/main-menu.vue'
import {DefineComponent} from "vue";

const mainMenu = MainMenu as unknown as DefineComponent;

@Options({
    components: {
        IonApp,
        IonRouterOutlet,
        IonPage,
        mainMenu,
    }
})
export default class App extends Vue  {

}
