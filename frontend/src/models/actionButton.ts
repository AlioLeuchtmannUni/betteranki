export default class ActionButton {

    public constructor(
        public iconName: string,
        public handler: VoidFunction,
    ){}
}