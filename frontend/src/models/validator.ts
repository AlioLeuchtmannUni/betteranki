import {ValidatorFunction} from "@/constants";


export class Validator {

    constructor(public validationFunction: ValidatorFunction) {}

    public validate(text:string): string | undefined {
        return this.validationFunction(text);
    }


    public static required(): Validator{
        return new Validator((text:string)=>{
           if(text === ''){
               return 'Required';
           }
           return undefined;
        });
    }

    public static min(min:number): Validator{
        return new Validator((text:string)=>{
            if(text.length < min){
                return `Min length is ${min}`;
            }
            return undefined;
        });
    }

    public static max(max:number): Validator{
        return new Validator((text:string)=>{
            if(text.length > max){
                return `Max length is ${max}`;
            }
            return undefined;
        });
    }

}