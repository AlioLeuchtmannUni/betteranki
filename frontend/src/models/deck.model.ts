import Tag from "./tag.model";
import Card from "@/models/card.model";


export default class Deck {

    constructor(
        public name: string,
        public description: string,
        public tags: Tag[],
        public favourite: boolean,
        public cards?: Card[],
        public id?: number,
        public shared?: boolean
    ){}

    public asPostObject(): Deck {
        return new Deck(this.name,this.description,this.tags,this.favourite);
    }
}
