
export default class CardMeta {

    constructor(
        public card_id: number,
        public score: number,
    ){}
}