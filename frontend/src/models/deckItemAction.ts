import Deck from "@/models/deck.model";

export type deckAction = (deck: Deck) => void;

export class DeckItemAction {

    public constructor(
        public label: string,
        public clickHandler: DeckItemAction,
        public iconName: string,
    ) {
    }

}