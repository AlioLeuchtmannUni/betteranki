export default class StatsPair {

    constructor(
        public descriptor: string,
        public value: number,
    ){}
}