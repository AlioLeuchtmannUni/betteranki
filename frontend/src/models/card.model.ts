import Deck from "./deck.model";


export default class Card {

    constructor(
        public name: string,
        public question_html: string,
        public answer_html: string,
        public deck_id?: string,
        public id?: string,
    ){}
}