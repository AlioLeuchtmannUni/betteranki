
### TODOS:

OPTIONAL
- [] edit button in menu wenn lern mode
- [] Backend Funktion für filtern von Decks nach Titel
- [] Backend Funktion für filtern von Decks nach tag
- [] caching
- [] Tags in Backend müssen neu erstellt werden bei copy von shared
- [] Backend paging
- [] last learned anstelle von favourites
- [] make nicer Pop ups
- [] title optional bei cards
- [] close open accordions on click of other
- [] ios design
- [] dark mode design checken
- [] tag management, own screen
- [] account page, avatar in app bar, password change ...
- [] KI die vor teilen von decks prüft ob unangebrachte inhalte enthalten sind 
- [] standard account speicher limitierung
- [] ToolBar feste optionen überlegen, blockiere kritische Optionen
- [] click 
- [] Preview of Deck before download


### Design

https://miro.com/app/board/o9J_luZW53Q=/?share_link_id=349049869650

![img.png](img.png)

## Links

ionic icons
https://ionic.io/ionicons

## Requirements
npm installieren // ein mal 

npm install -g @ionic/cli // ein mal

## Start up

ionic serve

## On Device

npm i -g native-run // ein mal

Gerät anschließen

ionic capacitor run android
-> Gerät wählen

### Schreibweisen

## Folder Structure:

### Globale css Klassen und Theming
BetterAnki/src/theme/variables.css

### Neue Routen für Komponenten
BetterAnki/src/router/index.ts

### Wiederverwendete Componenten
in frontend/src/components 

### Views für Seiten
in frontend/src/views/

### Neue Page erstellen 

Componente wie in Punkt ### Component Structure beschrieben erstellen

Pages

BetterAnki/src/router/index.ts

zu const routes: Array<RouteRecordRaw> = [] hinzufügen
>>>
{
path: '/home',
name: 'Home',
component: DiscoverScreen as unknown as VNode
},
>>>


### Component Structure


#### HTML Code in .vue File, importiert typescript file

>>> <template> muss Components / Pages umgeben
>>> <template><ion-page> muss Page umgeben

<template>
  <div ref="quillRef" id="quillRef"></div>
</template>


<script lang="ts" src="./quill-editor.ts"></script>

#### Frontend Logik

@Options({
name: 'QuillEditor',
components: {
  ... use Components defined here ... 
  ... e.g IonButton ...
})
export default class QuillEditor extends Vue  { }


    @Ref('quillRef') quillRef!: HTMLDivElement; 

    public editor: Quill;

    @Prop({default: ''}) value!: string;


### Custom componente nutzen beispiel:

import FloatingActionSheetButton from "@/components/layout/floatingActionSheetButton/FloatingActionSheetButton.vue";
const floatingActionSheetButton = FloatingActionSheetButton as unknown as DefineComponent;

@Options({
name: 'DiscoverScreen',
components: {
floatingActionSheetButton,
}
})

### Frontend style

in seperatem css File das im ts File importiert wird


### Nutzung einer Componente / Seite:

#### importieren
import RichTextEditor from "@/components/input/richTextEditor/RichTextEditor.vue";
#### exlpizite typ convertierung
const richTextEditor = RichTextEditor as unknown as DefineComponent;

@Options({
name: 'DeckEditPage',
components: {
#### add to components
richTextEditor,
}
})
export default class DeckEditPage extends Vue  {
...
}

            <rich-text-editor
                :hidden="!questionState"
                ref="questionEditor"
                editor-content-gets-passed-here="<p> This is the Question Card container ! </p>"
            ></rich-text-editor>


### INFOS on decorators:

https://github.com/kaorun343/vue-property-decorator

erlaubt nutzung von public variablen der Klasse
anstelle sie erst in data() { return ...} zu packen

anstelle von props{} reicht ein @Prop

... und so weiter
