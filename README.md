## BetterAnki

## Clone under Windows ##

When cloning under Windows it is advised to disable autocrlf as otherwise depending on the Git-Version some files will be converted to CRLF.
The backend Docker container does not execute successfully when the Bash-Scripts get converted to CRLF.

Use this command when cloning to avoid this issue:

SSH:
```bash
git clone --config core.autocrlf=false git@gitlab.com:AlioLeuchtmannUni/betteranki.git
```

HTTPS:
```bash
git clone --config core.autocrlf=false https://gitlab.com/AlioLeuchtmannUni/betteranki.git
```

### Backend

#### Requirements
- docker compose

#### Usage

```bash
docker-compose -f backend/backend-docker-compose.yml up
```
Open http://localhost:8080 in your browser.

### Frontend
 
#### Requirements

Docker or NodeJS + npm is required.

#### Installation & Execution

##### Using Docker

```bash
docker compose -f frontend/docker-compose.yml up
```

##### Without Docker

```bash
cd frontend
npm install -g cordova @ionic/cli
npm install

ionic serve
```

#### Usage

Open http://localhost:8100 in your browser.

Gehe in die Entwickler Einstellungen

![img_2.png](img_2.png)

Click jetzt auf das handy icon: 2tes Icon von Links in der Entwickler ansicht

![img.png](img.png)

#### Back-End Unittests

Python needs to be installed.
Install coverage using `pip install coverage`.

```bash
cd backend
coverage run -m unittest discover
```
