

## Chronoligischer ablauf

### Oktober

#### Gedanken zur Architektur
- Vue gegeben
- Idee eine App zu entwickeln -> durch den Constraint vue kam nur ein Framework in Frage das in Kombimation mit vue Funtktioniert -> Wahl viel auf Ionic
- Weitere Gründe für Ionic: Ich habe schon Erfahrungen mit Angular-Ionic, Ionic ist schnell, Ionic ist Plattform unabhängig
- Durch Mehrheitsentscheid viel die Wahl für das Backend auf Django :(
- Wir haben bei der Frontend Entwicklung weiterhin großen Wert auf Kapselung gelegt und mit Komponenten gearbeitet
- Verwendung von vue-property-decorator Package und vue class component Begründung:
-> Diese Packages erlauben eine meiner Meinung nach schönere Strucktur des Codes
-> Das typescript File ist seperat von dem html code und somit übersichtlicher
-> public Variablen können einfach genutzt werden anstelle sie anders wo mit javascript zu exportieren
-> @VModel erlaubt einfaches 2 Way Binding
-> kompakterer Code
-> mir vertrautere, Angular ähnlichere Struktur
-> Nachteil: nicht kompatibel mit neuesten Vue Versionen


#### Commits:

- Floating Action Button Componente, der man Aktionen übergeben kann
- Tutorials zur definierung von Komponenten und Views im Readme


### November:

- Customizable Input Element Komponente
- Rich Text Editor Komponente
- umstrucktierung der Ordner Strucktur
- EditCardPage begin
- docker compose für backend
- register, login page
- form validation

Probleme: anfangs muss man sicher erst einmal an Vue gewöhnen 

### Dezember
- route Protection -> nur Authorisierte Nutzer können bestimmte routen sehen -> sonst umleitung zu login
- add tags to deck creation
- Card item Componente
- Deck item Componente
- Deck Overview View
- Favourite Funktionalität bei Decks
- und weitere Design verbesserungen, fixes ...


### Januar
- filter und suche nach decks
- behebung von github issues 13,14,15,16,18
- card item design änderung -> dropdown um farge und antwort einer Karte in der Deckansicht zu sehen für bessere usability
- fixes und design verbesserungen
- fix routing Probleme durch update der Versionen von ionic/vue und ionic/vue-router


### Abschließende Anmerkung:
- diese Auflistung gibt nicht alle meiner über 100 Commits wieder, da ich sie teilweise nicht gut benannt habe, und der Aufwand den nutzen nicht recht fertigt weiter ins Detail zu gehen


Es gibt einige Dinge die wir aus Zeitgründen weg gelassen haben da wir der Meinung sind das wir schon überdurchschnittlich viel Zeit investiert haben:
- Offline Modus -> lokales Speichern der Daten und gelegentliche Synchronisation
- Learn Algortihmus der anhand der abgegebenen Scores Die zu lernenden Karten bestimmt
- Paging: Von Anfang an im Gespräch gewesen aber aus Zeitgründen weg gelassen -> so wäre die App bei sehr vielen Decks mit vielen Karten nicht nutzbar 
- Caching
- Limitierung des Speichers pro Nutzer / Pro Version -> sonst missbrauch als free Cloud
- Validierung der Inhalte durch eine K.I vor dem Speichern auf dem Server um Missbrauch und Hetze zu vermeiden
- Backend Endpunkt zum filtern nach Deck Titel oder Tags -> da bei paging dann natürlich nicht alle im Frontend verfügbar
- Weitere Dashboard Funktionalität
- Optimierungen -> Teilweise wird bei jedem updated() eine neue Request gemacht -> an diesen Stellen wären Dinge wie Webhooks Sinnvoller um nicht unnötig Daten zu laden

### Was habe ich gelernt

Die Arbeit im Team ist deutlich Zeitintensiver und braucht mehr Abstimmung als die Arbeit alleine.

Ich fand es schön auch mal ein anderes Webframework kennen zu lernen als Angular,
in Zukunft werde ich dennoch auf Vue verzichten wenn ich kann,
wobei es sich aber um eine persönliche Präfernz handelt.
Einige Gründe: 

- 1. 2 Way binding, Property Binding, Event Binding deutlich einfacher, und nur änderungen in View nötig nicht im Model
<my-component [(foo)]="bar"> </my-component>   <!-- Wobei "bar" eine public Variable der zugehörigen typescript Komponente ist -->

2. Module Architecture
- Angular ermöglicht es Module zu erstellen die sich importieren lassen,was eine schönere Strucktur ermöglicht und vermeidet in jeder Componente die selben Module zu importieren
-> so kan ich mir Beispielsweise ein MaterialModul erstellen das alle MaterialComponenten importiert die ich brauche und diese mit einem einzigen import in einer anderen Komponente verfügbar machen
-> Routing Modules routen wie /deck/edit , /deck/create , /deck/view ...
lassen sich zusammen fassen indem in dem in einer deckrouting Componente Child routes definiert werden
somit hat man nicht am ende in einer datei 100 Routen und mann kan besser kapseln

Ich denke Vue ist leichter zu lernen aber das Angular einen Anfänger besser in Richtung Komponenten Architektur zwingt was ich als Vorteil sehe, 
da dies so oder so ein muss ist und es keinen Grund gibt darauf zu verzichten.
Desweiteren ist Angular besser für große Anwendungen während Vue besser für kleinere geeignet ist.

Größere Fehlentwicklungen haben wir nicht erlebt, sondern eher einige kleine Bugs gehabt.
Ein Gamebreaker wäre es jedoch gewesen wenn sich der interne Routing bug durch das Versionsupdate nicht behoben lassen hätte,
die App wäre dan schlicht und einfach nutzlos da Teilweise einfach nur ein weißer Hintergrund angezeigt wurde und man auf dem Mobilgerät die App neu starten müsste.



Alles in allem hat es mir großen Spaß gemacht und ich möchte mich für dieses Modul bedanken :)








