# Entwicklertagebuch

## Oktober

- Im Oktober beschäftigte ich mich größtenteils damit, die Grundstruktur des Backends aufzubauen. Darunter zählt die Konfiguration der Django App sowie die Einbindung bzw. die Einarbeitung in das REST Framework Django Ninja
- Des Weiteren entwickelten wir in Zusammenarbeit anhand der vorher festgelegten Anforderungen, den ersten Entwurf des Datenmodells. Im Verlauf der Entwicklung gab es hin und wieder kleinere Änderungen am Datenmodell, um neue Features zu implementieren.
- Da sich die App online synchronisieren soll, war es nötig ein Account System für das Backend zu nutzen. Django bringt dies von Haus aus bereits mit sich. Allerdings wurde Django ursprünglich für das serverseitige Rendern der Website entwickelt, worauf auch das mitgelieferte Authentifikationssystem ausgelegt ist. Da wir mithilfe von Ninja Django als REST Schnittstelle verwendeten, konnten wir das mitgelieferte Auth-System nur zum Teil verwenden. Letztlich fiel unsere Entscheidung darauf JWT (JSON Web Token) zu verwenden, da es ein weit verbreiteter und offener Standard ist. Das JWT System implementierte ich mithilfe vom django-ninja-jwt package.


## November

- Im November erstellte ich den Großteil der API Endpunkte (CRUD) für die Datenmodelle Decks, Cards und Tags.
- Außerdem stellte ich das Login System fertig, welches es via API ermöglicht, Django Nutzeraccounts zu erstellen bzw. zu bearbeiten und diese anhand des JSON Webtoken bei API Requests zu identifizieren.
- Auch das Feature zum Ändern/ Zurücksetzen des Passworts via Email Link stellte ich fertig
- Des Weiteren strukturierte ich alle zuvor geschriebenen API Endpunkte in klassenbasierte Controller um, sodass diese in den API Docs besser verständlich unterteilt und somit besser wartbar sind.


## Dezember

- Im Dezember widmete ich mich der Entwicklung des Lernviews. Dabei implementierte ich das User Interface, als auch die Funktionalität des Lernviews (Abfrage der Karten, Verdecken/ Aufdecken der Lösung, Senden der Antwortstatistik).
- Um die Antworten, die der Nutzer während des Lernens gibt (Easy, Good, Hard, Again) zu speichern und später weiterzuverarbeiten, implementierte ich die entsprechenden API Enpunkte für die Metadaten (CardMeta)
- Außerdem bearbeitete ich einige API Endpunkte so, dass sich das Handling der Requests im Frontend einfacher gestaltet und insgesamt weniger Requests gestellt werden müssen. Bsp.: Das Erstellen von neuen Tags on the fly während der POST Operation auf /decks
- Auch passte ich das Datenmodell an, sodass Decks favorisiert werden und somit als erstes auf der Startseite erscheinen
- Ansonsten behob ich kleinere Bugs in einigen der API Endpunkte und dokumentierte den Aufbau der Backends sowie nützliche Informationen zur Weiterentwickeln der API Endpunkte.


## Januar

- Im Januar gestaltete ich den zuvor nur provisorisch eingebundenen Account View inkl. Funktionalität zum Löschen des Accounts neu.
- Ich passte das UI entsprechend an, sodass das Teilen von Decks auch wieder deaktiviert werden kann. Ebenfalls habe ich das Backend um die Funktion des Teilens/ Zurückziehen des Teilens erweitert
- Des Weiteren implementierte ich im Frontend die Statistiken für die Anzahl der gelernten Karten sowie die Statistik für die abgegebenen Antworten (Easy, Hard, ...). Das Backend habe ich um die entsprechenden Statistik Endpunkte erweitert.
- Ansonsten habe ich letzte Bugs im Lernview und im Account gefixt sowie kleinere kosmetische Anpassungen vorgenommen, z.B.
  - Hinzufügen von Ionic Badges welche im Deck View die Anzahl an Karten für das entsprechende Deck angeben
  - Anpassung von Icons und Schriftgröße